/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Хэдер
 */

import UIKit

class FPHeaderView: UIView {
  //*** Labels
  @IBOutlet weak var tittleLabel: UILabel!
  //*** Buttons
  @IBOutlet weak var showHideSideViewButton: UIButton!

  private var _animationDuration    = cBaseViewController.SidePanel.fullAnimDur
  private var _buttonDirectionRight = true// стрелочка на кнопке направлена вправо
  
  //*** Анимация вращения кнопки
  func performButtonRotateAnimation(rotationAngle: CGFloat, animated: Bool) {
    var animationDuration: TimeInterval = _animationDuration
    
    if !animated {
      animationDuration = 0
    }
    
    if rotationAngle == 2 * .pi {
      _buttonDirectionRight = false
    } else if rotationAngle == -.pi {
        _buttonDirectionRight = true
      }
    
    UIView.animate(withDuration: animationDuration) {
      self.showHideSideViewButton.transform = CGAffineTransform(rotationAngle: rotationAngle)
      
      self.layoutIfNeeded()
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Нажали на кнопку для бокового меню
  @IBAction func sideViewButton(_ sender: UIButton) {
    var velocityX: CGFloat     = 1
    var rotationAngle: CGFloat = 2 * .pi
    var hide: Bool             = false
    
    guard let baseCnt = getParentObject(FPBaseViewController.self) else {
      return
    }
    
    if !_buttonDirectionRight {
      rotationAngle = -.pi
      velocityX     = -1// ставим смещение отрицательным, т.к. вызываем анимацию исчезновения
      hide          = true
    }
    
    baseCnt.performSideMenuAnimation(animationDuration: _animationDuration,
                                     hide: hide,
                                     velocityX: velocityX,
                                     completitionBlock: nil)
    baseCnt.headerView?.performButtonRotateAnimation(rotationAngle: rotationAngle, animated: true)
  }
  //-----------------------------------------------------------------------------------
  
  //*** Установка заголовка
  func setTittle(category: EnRequest) {
    let constants = cBaseViewController.Header.self
    
    switch category {
      case .funnyPictures:
        tittleLabel.text = constants.funny
      case .memes:
        tittleLabel.text = constants.memes
      case .quotes:
        tittleLabel.text = constants.quotes
      case .random:
        tittleLabel.text = constants.random
    }
  }
  //-----------------------------------------------------------------------------------
}
