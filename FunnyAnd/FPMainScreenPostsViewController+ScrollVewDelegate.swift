/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Делаем запросы на новые порции постов, изменяем позицию хэдера
 */

import UIKit

extension FPMainScreenPostsViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    var contentSizePosition      = scrollView.contentSize.height
    var minPosForDownloadNewData = scrollView.contentOffset.y + cBaseViewController.heightMultiplier * scrollView.frame.height
    
    if scrollView == detailPostView?.collectionViewDetailPost {
      contentSizePosition      = scrollView.contentSize.width
      minPosForDownloadNewData = scrollView.contentOffset.x + cBaseViewController.heightMultiplier * scrollView.frame.width
      
      detailPostView?.setPanGesturesInVisibleCells(enabled: false)
      detailPostView?.setCurrCellData()
    } else {
        setupHeaderPositionRelativeToScroll(scrollYOffset: scrollView.contentOffset.y)// изменение позиции хедера
      }
    
    if !postData.isEmpty && (contentSizePosition <= minPosForDownloadNewData) {
      // запрос на загрузку новой порции постов
      if !presenter.isDataDownloadedAtThisMoment {
        presenter.downloadNewData(type: categoryType)
      } 
    }
  }
  //-----------------------------------------------------------------------------------
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    scrollEnded()
    
    if scrollView.contentOffset.y <= 0 {// потянули вниз в начале скрола
      pullDownEnded(animated: true)
    }
    
    detailPostView?.setPanGesturesInVisibleCells(enabled: true)
  }
  //-----------------------------------------------------------------------------------
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    scrollEnded()
    
    detailPostView?.setPanGesturesInVisibleCells(enabled: true)
  }
  //-----------------------------------------------------------------------------------
}
