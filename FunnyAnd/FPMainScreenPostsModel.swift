/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Модель главного экрана
 */

import AlamofireImage

class FPMainScreenPostsModel: FPMainScreenPoststModelProtocol {
  weak var presenter: FPMainScreenPostsModelHandleUpdateProtocol?
  
  private let _dataDownloadGroup           = DispatchGroup()
  private var _postsArr: [FPSPostData]     = []// данные funnyPictures
  private var _catPostsData                = FPSCatData()
  private var _category: EnRequest         = .funnyPictures
  private var _offset: integer_t           = 0// начальное смещение
  private var _noInternet: Bool            = false// если в процесс обращения к серверу пропал интернет

// MARK: - constructor
  
  required init(presenter: FPMainScreenPostsModelHandleUpdateProtocol) {
    self.presenter = presenter
    
    start()
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - first download
  
  //*** Метод загружает данные постов и изображения для начального экрана
  func start() {
    
// MARK: - TODO: копипаста
  
    let initialOffset: integer_t = 0

    _dataDownloadGroup.enter()
    FPNetworkQueries.getNewPostData(offset: initialOffset, funnyRequest: .funnyPictures) { funnyPicturesData, request in
      // получили данные постов
      defer { self._dataDownloadGroup.leave() }
      
      if funnyPicturesData == nil {
        self._noInternet = true
        
        return
      }
      
      // здесь данные гарантированно есть
      guard !funnyPicturesData!.isEmpty else {
        assert(false, cErrorHandling.Messages.emptyPosts + request)
      }
      
      self._catPostsData.funny = funnyPicturesData!
      
      // загрузка данных изображения
      for i in 0...(funnyPicturesData!.count - 1) {
        self._dataDownloadGroup.enter()// входим в группу на каждой итерации
        self.downloadImageData(media: funnyPicturesData![i].featuredMedia, category: .funnyPictures) { imageData in
          if imageData == nil {
            self._noInternet = true
          } else {
              self._catPostsData.funny[i].Image = imageData!
            }
        }
      }
    }
  
    // memes
    _dataDownloadGroup.enter()
    FPNetworkQueries.getNewPostData(offset: initialOffset, funnyRequest: .memes) { memesData, request in
      defer { self._dataDownloadGroup.leave() }
      
      if memesData == nil {
        self._noInternet = true
        
        return
      }
      
      guard !memesData!.isEmpty else {
        assert(false, cErrorHandling.Messages.emptyPosts + request)
      }
      
      self._catPostsData.memes = memesData!
      
      // загрузка данных изображения
      for i in 0...(memesData!.count - 1) {
        self._dataDownloadGroup.enter()// входим в группу на каждой итерации
        self.downloadImageData(media: memesData![i].featuredMedia, category: .memes) { imageData in
          if imageData == nil {
            self._noInternet = true
          } else {
              self._catPostsData.memes[i].Image = imageData!
            }
        }
      }
      
      //self.downloadImageData(media: (memesData.first?.featuredMedia)!, category: .memes, post: memesData.first!)// загрузка данных изображения
    }
    
    // quotes
    _dataDownloadGroup.enter()
    FPNetworkQueries.getNewPostData(offset: initialOffset, funnyRequest: .quotes) { quotesData, request in
      defer { self._dataDownloadGroup.leave() }
      
      if quotesData == nil {
        self._noInternet = true
        
        return
      }
      
      guard !quotesData!.isEmpty else {
        assert(false, cErrorHandling.Messages.emptyPosts + request)
      }
      
      self._catPostsData.quotes = quotesData!
      
      // загрузка данных изображения
      for i in 0...(quotesData!.count - 1) {
        self._dataDownloadGroup.enter()// входим в группу на каждой итерации
        self.downloadImageData(media: quotesData![i].featuredMedia, category: .quotes) { imageData in
          if imageData == nil {
            self._noInternet = true
          } else {
              self._catPostsData.quotes[i].Image = imageData!
            }
        }
      }
      
      //self.downloadImageData(media: (quotesData.first?.featuredMedia)!, category: .quotes, post: quotesData.first!)// загрузка данных изображения
    }
    
    // random
    _dataDownloadGroup.enter()
    FPNetworkQueries.getNewPostData(offset: initialOffset, funnyRequest: .random) { randomData, request in
      defer { self._dataDownloadGroup.leave() }
      
      if randomData == nil {
        self._noInternet = true
        
        return
      }
      
      guard !randomData!.isEmpty else {
        assert(false, cErrorHandling.Messages.emptyPosts + request)
      }
      
      self._catPostsData.random = randomData!
      
      // загрузка данных изображения
      for i in 0...(randomData!.count - 1) {
        self._dataDownloadGroup.enter()// входим в группу на каждой итерации
        self.downloadImageData(media: randomData![i].featuredMedia, category: .random) { imageData in
          if imageData == nil {
            self._noInternet = true
          } else {
              self._catPostsData.random[i].Image = imageData!
            }
        }
      }
      
      //self.downloadImageData(media: (quotesData.first?.featuredMedia)!, category: .quotes, post: quotesData.first!)// загрузка данных изображения
    }
    
    _dataDownloadGroup.notify(queue: .main) { [weak self] in
      guard let strongSelf = self else {
        return
      }
      
      // все загрузки завершены, передаем данные в presenter
      strongSelf._offset = integer_t(strongSelf._catPostsData.funny.count)
      
      strongSelf.presenter?.didInitialDataDownloaded(categoriesBuff: strongSelf._catPostsData, noInternet: strongSelf._noInternet)
      
      strongSelf._noInternet = false
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - data download
  
  //*** Загрузка данных изображения и самой картинки
  private func downloadImageData(media: int_fast64_t, category: EnRequest, completion: @escaping (FPSImageData?) -> Void) {
    FPNetworkQueries.getImage(featuredMedia: media) { imageData in// получили данные изображения
      completion(imageData)
      self._dataDownloadGroup.leave()// выходим из группы если не загружаем изображение
      // загружаем изображение и кэшируем изображение(не нужно если используем стандартное расширение для UIImageView в AlamofireImage)
      //FPSharedApplication.getInstance().downloadImage(url: imageData.url!, dispatchGroup: self._dataDownloadGroup) { image in }// url здесь точно существует
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Новая порция постов
  func downloadNewData(type: EnRequest) {
    _category = type
    
    _postsArr.removeAll()
    _dataDownloadGroup.enter()
    FPNetworkQueries.getNewPostData(offset: _offset, funnyRequest: type) { data, request in
      // получили данные постов
      defer { self._dataDownloadGroup.leave() }
      
      if data == nil {
        self._noInternet = true
        
        return
      }
      
      // здесь данные гарантированно есть, но все равно проверяем
      guard !data!.isEmpty else {
        assert(false, cErrorHandling.Messages.emptyPosts + request)
      }
      
      if self._postsArr.isEmpty {
        self._postsArr = data!
      }
      
      for i in 0...(self._postsArr.count - 1) {
        self._dataDownloadGroup.enter()// входим в группу на каждой итерации
        self.downloadImageData(media: self._postsArr[i].featuredMedia, category: .funnyPictures){ imageData in
          if imageData == nil {
            self._noInternet = true
          } else {
              self._postsArr[i].Image = imageData!
            }
        }
      }
    }
    
    _dataDownloadGroup.notify(queue: .main) { [weak self] in
      guard let strongSelf = self else {
        return
      }
      
      // все загрузки завершены, передаем данные в presenter
      strongSelf._offset += integer_t(strongSelf._postsArr.count)// обновляем смещение
      
      strongSelf.presenter?.didDataDownloaded(data: strongSelf._postsArr, type: type, noInternet: strongSelf._noInternet)
      
      strongSelf._noInternet = false
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - likes
  
  //*** Запрос для лайков
  func performVote(parametrs: [String : Any], like: EnVote) {
    FPNetworkQueries.performPOST(parametrs: parametrs) { success in
      // здесь запрос обработан успешно
      var res: EnVoteSuccess = .up
      
      if like == .down {
        res = .down
      }
      
      if success != .like_success {
        res = .alreadyVoted
      }
      
      self.presenter?.didVoteSuccess(res)
    }
  }
  //-----------------------------------------------------------------------------------
}
