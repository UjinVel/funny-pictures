/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Обработка событий презентера(после обновления модели)
 */

import UIKit

extension FPMainScreenPostsViewController: FPMainScreenPostsViewHandleUpdateProtocol {
  //*** Начальные данные(посты+категории) готовы для отображения
  func didInitialDataDownloaded(categoriesBuff: FPSCatData, noInternet: Bool) {
    if noInternet {
      self.present(ErrorAlert.getAlert(message: cErrorHandling.Messages.noInternet),
                                      animated: false,
                                      completion: nil)
      
      return
    }
    
    if changeCategoryIfNeeded() {
      return
    }
    
    switch categoryType {
      case .funnyPictures:
        postData = categoriesBuff.funny
      case .memes:
        postData = categoriesBuff.memes
      case .quotes:
        postData = categoriesBuff.quotes
      case .random:
        postData = categoriesBuff.random
    }
    
    categoriesData = categoriesBuff
    
    collectionViewPosts.reloadData()// обновляем главную таблицу
    detailPostView?.collectionViewDetailPost.reloadData()// обновляем таблицу для детального поста
  }
  //-----------------------------------------------------------------------------------
  
  //*** Новая порция постов готова для отображения
  func didDataDownloaded(data: [FPSPostData], noInternet: Bool) {
    if noInternet {
      self.present(ErrorAlert.getAlert(message: cErrorHandling.Messages.noInternet),
                   animated: false,
                   completion: nil)
      
      return
    }
    
    if changeCategoryIfNeeded() {
      return
    }
    
    postData.append(contentsOf: data)
    
    var indexPathes: [IndexPath] = []
    
    DispatchQueue.global(qos: .default).async {
      indexPathes = data.map{ IndexPath(item: self.postData.index(of: $0)!, section: 0) }// создаем масиив indexPath(_postData гарантированно содержит все элементы data)
      
      DispatchQueue.main.async {
        // обновляем таблицу для детального поста
        self.collectionViewPosts.performBatchUpdates({ () -> Void in
          self.collectionViewPosts.insertItems(at: indexPathes)
        }, completion: nil)
        
        // обновляем главную таблицу
        self.detailPostView?.collectionViewDetailPost.performBatchUpdates({ () -> Void in
          self.detailPostView?.collectionViewDetailPost.insertItems(at: indexPathes)
        }, completion: nil)
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Запрос на лайк прошел успешно
  func didVoteSuccess(_ success: EnVoteSuccess, postIndex: Int) {
    // проверяем, не висит ли уведомление сейчас
    for vote in view.subviews {
      if vote is FPVoteResult{
        return
      }
    }
    
    guard let voteRes: FPVoteResult = .getNibObject(fileName: FPVoteResult.self) else {
      return
    }
    
    // обновляем данные лайков в буфере
    if success == .up {
      postData[postIndex].likesCount += 1
    } else if success == .down {
        postData[postIndex].likesCount -= 1
      }
    
    detailPostView?.setCurrCellData()// обновляем ячейку
    
    voteRes.performAlert(superView: view, success: success)// выводим сообщение о результате лайка
  }
  //-----------------------------------------------------------------------------------
}
