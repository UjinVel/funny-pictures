/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import AlamofireImage

class FPPostCell: FPBaseCell {
  //*** Views
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var footerView: UIView!
  //*** Labels
  @IBOutlet weak var tittle: UILabel!
  //*** Constraints
  @IBOutlet weak var imageViewLeading: NSLayoutConstraint!
  @IBOutlet weak var imageViewTrailing: NSLayoutConstraint!
  @IBOutlet weak var tittleTop: NSLayoutConstraint!
  @IBOutlet weak var tittleBottom: NSLayoutConstraint!
  @IBOutlet weak var imageViewBottom: NSLayoutConstraint!
  @IBOutlet weak var buttonsContentViewHeight: NSLayoutConstraint!

// MARK: Uivew overrided
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    // установка констреинтов
    let constants                     = cMainScreenPostsViewController.CollectionViewPosts.PostCell.self
    imageViewLeading.constant         = constants.imageViewLeading
    imageViewTrailing.constant        = constants.imageViewTrailing
    tittleTop.constant                = constants.tittleTop
    tittleBottom.constant             = constants.tittleBottom
    imageViewBottom.constant          = constants.imageViewBottom
    buttonsContentViewHeight.constant = constants.buttonsContentViewHeight
    
    setupFooter()
  }
  //-----------------------------------------------------------------------------------
  
  override func setImage(hiden: Bool) {
    imageView.isHidden = hiden
  }
  //-----------------------------------------------------------------------------------

// MARK: Initial setup
  
  func setup(data: FPSPostData, index: Int) {
    // загружаем или получаем из кэша изображение(нарушение MVP)
    imageView.af_setImage(withURLRequest: URLRequest(url: data.Image.url!), placeholderImage: UIImage(named: cImages.placeholder))
    
    tag                = index
    tittle.text        = data.tittle
    imageView.isHidden = false
  }
  //-----------------------------------------------------------------------------------
  
  //*** Настройка футера
  private func setupFooter() {
    guard let footer = UIView.getNibObject(fileName: FPPostFooter.self) else {
      return
    }
    
    footer.setupPosition(superView: footerView)
    
    footer.delegate = self
  }
  //-----------------------------------------------------------------------------------
}
