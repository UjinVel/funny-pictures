/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

extension FPCollectionPostsReusableHeaderView: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if categoriesData.isEmpty() {
      return 0
    }
      
    return Int(cMainScreenPostsViewController.CollectionViewPosts.CollectionViewCategories.categoriesOnScreen)
  }
  //-----------------------------------------------------------------------------------
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cMainScreenPostsViewController.Reuseidentifiers.categoriesCell, for: indexPath) as? FPCategoriesCell else {
      assert(false, cErrorHandling.Messages.cantCastErr)
    }
    
    var data: [FPSPostData] = []
    var type: EnRequest     = .funnyPictures
    let tittle              = cBaseViewController.Header.self
    
    switch indexPath.row {
      case 0:
        data = categoriesData.funny
      
        cell.tittleLabel.text = tittle.funny
      case 1:
        data = categoriesData.memes
        type = .memes
        
        cell.tittleLabel.text = tittle.memes
      case 2:
        data = categoriesData.quotes
        type = .quotes
      
        cell.tittleLabel.text = tittle.quotes
      default:
        assert(false, cErrorHandling.Messages.unexpectedElement)
    }
    
    if data.isEmpty {
      assert(false, cErrorHandling.Messages.outOfRange)
    }
    
    cell.setup(data: data.first!, currCategory: type)
    
    return cell
  }
  //-----------------------------------------------------------------------------------
}
