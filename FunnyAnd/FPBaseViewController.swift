/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Базовый класс вью контроллера, наследуют все экраны
 */

import UIKit

enum EnDirectionPan {
  case right
  case left
}

class FPBaseViewController: UIViewController {
  //*** StatusBar view
  let statusBarView = UIView()
  //*** Header
  lazy var headerView: FPHeaderView? = .getNibObject(fileName: FPHeaderView.self)
  private var _headerViewTop: NSLayoutConstraint?// верхняя позиция _headerView
  private var _scrollContentOfsetY: CGFloat?// значение смещения скрола по у
  private var _headerViewTopCurr: CGFloat?// значение констреинта после окончания скрола
  //*** Side view
  private lazy var _sideView: FPSideView? = .getNibObject(fileName: FPSideView.self)
  //*** Handle pan for _sideView
  private var _sideBarBackgroundView: UIVisualEffectView?// привязана к правой стороне _sideView
  private var _sideBarLeading: NSLayoutConstraint?// констреинт, который изменяем во время пана
  private var _sidebarLeadingCurrentValue: CGFloat = 0// значение после отпускания пальца(вьюха видна или нет)
  private var _handlePanXLocationInView: CGFloat   = cBaseViewController.SidePanel.startHandlePan// промежуток от левого края экрана, где начинает срабатывать обработка пана
  private var _firsTapeXLocationInView: CGFloat?// первое значение, откуда начинаеться движение
  private var _directionPan: EnDirectionPan?// первоначальное нааправление паана для бокового меню
  
// MARK: - UIViewController overrided methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addPanGesture()
  }
  //-----------------------------------------------------------------------------------
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    setupElements()
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - Initial setup
  
  //*** Добавляем хэдер и боковое меню
  private func setupElements() {
    addHeader()
    addSideView()
    addStatusBarView()
    
    view.layoutIfNeeded()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Добавляем вьюху под статус бар(если есть), чтобы хэдер заезжал под нее(добавляем после хэдера)
  private func addStatusBarView() {
    if UIApplication.shared.statusBarFrame.height != 0 {
      view.addSubview(statusBarView)
    
      statusBarView.translatesAutoresizingMaskIntoConstraints = false
      
      // верх
      view.addConstraint(NSLayoutConstraint(item: statusBarView,
                                            attribute: .top,
                                            relatedBy: .equal,
                                            toItem: view,
                                            attribute: .top,
                                            multiplier: cBaseViewController.StatusBar.topMultiplier,
                                            constant: cBaseViewController.StatusBar.topConstant))
      // лево
      view.addConstraint(NSLayoutConstraint(item: statusBarView,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: view,
                                            attribute: .leading,
                                            multiplier: cBaseViewController.StatusBar.leadingMultiplier,
                                            constant: cBaseViewController.StatusBar.leadingConstant))
      // право
      view.addConstraint(NSLayoutConstraint(item: statusBarView,
                                            attribute: .trailing,
                                            relatedBy: .equal,
                                            toItem: view,
                                            attribute: .trailing,
                                            multiplier: cBaseViewController.StatusBar.trailingMultiplier,
                                            constant: cBaseViewController.StatusBar.trailingConstant))
      // высота
      statusBarView.addConstraint(NSLayoutConstraint(item: statusBarView,
                                                     attribute: .height,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .height,
                                                     multiplier: cBaseViewController.StatusBar.heightMultiplier,
                                                     constant: UIApplication.shared.statusBarFrame.height))// высота по размеру статус-бара
      
      statusBarView.backgroundColor = .white
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Добавляем боковую менюшку с Auto Layout
  private func addSideView() {
    guard _sideView != nil else {
      assert(false, cErrorHandling.Messages.loadNibErr + String(describing: FPSideView.self) + cFile.Extensions.xib)
    }
    
    // вьюха с кнопками
    if !view.subviews.contains(_sideView!) {
      view.addSubview(_sideView!)
    }
    
    _sideView?.translatesAutoresizingMaskIntoConstraints = false
    
    // ширина
    _sideView!.addConstraint(NSLayoutConstraint(item: _sideView!,
                                                attribute: .width,
                                                relatedBy: .equal,
                                                toItem: nil,
                                                attribute: .width,
                                                multiplier: cBaseViewController.SidePanel.widthMultiplier,
                                                constant: view.frame.width * cBaseViewController.SidePanel.widthScale))
    // лево
    _sidebarLeadingCurrentValue = -view.frame.width * cBaseViewController.SidePanel.widthScale// устанаавливаем текущее значение
    _sideBarLeading             = NSLayoutConstraint(item: _sideView!,
                                         attribute: .leading,
                                         relatedBy: .equal,
                                         toItem: view,
                                         attribute: .leading,
                                         multiplier: cBaseViewController.SidePanel.leadingMultiplier,
                                         constant: _sidebarLeadingCurrentValue)
    view.addConstraint(_sideBarLeading!)
    // вверх
    view.addConstraint(NSLayoutConstraint(item: _sideView!,
                                          attribute: .top,
                                          relatedBy: .equal,
                                          toItem: headerView!,
                                          attribute: .bottom,
                                          multiplier: cBaseViewController.SidePanel.topMultiplier,
                                          constant: cBaseViewController.SidePanel.topConstant))
    // низ
    view.addConstraint(NSLayoutConstraint(item: _sideView!,
                                          attribute: .bottom,
                                          relatedBy: .equal,
                                          toItem: view,
                                          attribute: .bottom,
                                          multiplier: cBaseViewController.SidePanel.bottomMultiplier,
                                          constant: cBaseViewController.SidePanel.bottomConstant))
    
    addSideViewBackground()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Добавляем вюху, покрывающую видимую часть экрана при вытаскивании бокового меню(привязана к правой части меню с Auto Layout)
  private func addSideViewBackground() {
    // мыло
    let blurEffect                           = UIBlurEffect(style: .dark)
    _sideBarBackgroundView                   = UIVisualEffectView(effect: blurEffect)
    _sideBarBackgroundView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    _sideBarBackgroundView?.alpha            = 0// установка значения alpha отличным от 1 у UIVisualEffectView может привести к неопределенному поведению
    
    if !view.subviews.contains(_sideBarBackgroundView!) {
      view.addSubview(_sideBarBackgroundView!)
    }
    
    _sideBarBackgroundView?.translatesAutoresizingMaskIntoConstraints = false
    
    // ширина
    _sideBarBackgroundView!.addConstraint(NSLayoutConstraint(item: _sideBarBackgroundView!,
                                                             attribute: .width,
                                                             relatedBy: .equal,
                                                             toItem: nil,
                                                             attribute: .width,
                                                             multiplier: cBaseViewController.SidePanel.Background.widthMultiplier,
                                                             constant: view.frame.height))
    // лево
    view.addConstraint(NSLayoutConstraint(item: _sideBarBackgroundView!,
                                          attribute: .leading,
                                          relatedBy: .equal,
                                          toItem: _sideView!,
                                          attribute: .trailing,
                                          multiplier: cBaseViewController.SidePanel.Background.leadingMultiplier,
                                          constant: cBaseViewController.SidePanel.Background.leadingConstant))
    // вверх
    view.addConstraint(NSLayoutConstraint(item: _sideBarBackgroundView!,
                                          attribute: .top,
                                          relatedBy: .equal,
                                          toItem: _sideView!,
                                          attribute: .top,
                                          multiplier: cBaseViewController.SidePanel.Background.topMultiplier,
                                          constant: cBaseViewController.SidePanel.Background.topConstant))
    // низ
    view.addConstraint(NSLayoutConstraint(item: _sideBarBackgroundView!,
                                          attribute: .bottom,
                                          relatedBy: .equal,
                                          toItem: view,
                                          attribute: .bottom,
                                          multiplier: cBaseViewController.SidePanel.Background.bottomMultiplier,
                                          constant: cBaseViewController.SidePanel.Background.bottomConstant))
  }
  //-----------------------------------------------------------------------------------
  
  //*** Добаляем хэдер с Auto Layout
  private func addHeader() {
    guard headerView != nil else {
      assert(false, cErrorHandling.Messages.loadNibErr + String(describing: FPHeaderView.self) + cFile.Extensions.xib)
    }
    
    if !view.subviews.contains(headerView!) {
      view.addSubview(headerView!)
    }
    
    // здесь _headerView точно существует
    headerView?.translatesAutoresizingMaskIntoConstraints = false
    
    // право
    view.addConstraint(NSLayoutConstraint(item: headerView!,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: view,
                                          attribute: .trailing,
                                          multiplier: cBaseViewController.Header.trailingMultiplier,
                                          constant: cBaseViewController.Header.trailingConstant))
    // лево
    view.addConstraint(NSLayoutConstraint(item: headerView!,
                                          attribute: .leading,
                                          relatedBy: .equal,
                                          toItem: view,
                                          attribute: .leading,
                                          multiplier: cBaseViewController.Header.leadingMultiplier,
                                          constant: cBaseViewController.Header.leadingConstant))
    // вверх
    _headerViewTop = NSLayoutConstraint(item: headerView!,
                                        attribute: .top,
                                        relatedBy: .equal,
                                        toItem: view,
                                        attribute: .top,
                                        multiplier: cBaseViewController.Header.topMultiplier,
                                        constant: UIApplication.shared.statusBarFrame.height + cBaseViewController.Header.topConstant)// добавляем высоту статус бара
    view.addConstraint(_headerViewTop!)
    // высота
    let height = NSLayoutConstraint(item: headerView!,
                                    attribute: .height,
                                    relatedBy: .equal,
                                    toItem: nil,
                                    attribute: .height,
                                    multiplier: cBaseViewController.Header.heightMultiplier,
                                    constant: cBaseViewController.Header.heightConstant)
    
    headerView?.addConstraint(height)

    headerView?.performButtonRotateAnimation(rotationAngle: .pi, animated: false)
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - SideView set delegate
  
  //*** Установка делегата для отслеживания событий нажатия кнопок на боковом меню
  func sideViewSetDelegate(_ delegate: SideViewDelegate) {
    _sideView?.delegate = delegate
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - Side menu pan
  
  //*** Добавляем гестуру для вытягивания бокового меню
  private func addPanGesture() {
    let pan                    = UIPanGestureRecognizer(target: self, action: #selector(handlePanEvent(gesture:)))
    pan.minimumNumberOfTouches = cBaseViewController.SidePanel.minimumNumberOfTouches
    
    view.addGestureRecognizer(pan)
  }
  //-----------------------------------------------------------------------------------
  
  //*** Обработка пана бокового меню
  func handlePanEvent(gesture: UIPanGestureRecognizer) {
    if _firsTapeXLocationInView == nil {
      _firsTapeXLocationInView = gesture.location(in: view).x
    }
    
    let translation = gesture.translation(in: view)
    let velocity    = gesture.velocity(in: view)
    
    if _directionPan == nil {
      if velocity.x > 0 {
        _directionPan = .right
      } else {
          _directionPan = .left
        }
    }
    
    //let alphaMin: CGFloat              = 0
    let alphaMax: CGFloat              = 1
    let sideBarLeadingVisible: CGFloat = 0// максимальное значчение, когда видна боковая панель
    let sideBarLeadingDef              = view.frame.width * cBaseViewController.SidePanel.widthScale// позиция по умолчаанию для констреинта левой стороны боковой панели
    
    if (_directionPan! == .right && _sideBarLeading?.constant == sideBarLeadingVisible) || (_directionPan! == .left && _sideBarLeading?.constant == -sideBarLeadingDef) || _firsTapeXLocationInView! >= _handlePanXLocationInView {
      _firsTapeXLocationInView = nil
      _directionPan            = nil
      
      return
    }
    
    if velocity.x > 0 && _sideBarLeading?.constant == sideBarLeadingVisible {
      _sideBarBackgroundView?.alpha = alphaMax
      _firsTapeXLocationInView      = nil
      
      return
    }
    
    if translation.x < sideBarLeadingDef {
      _sideBarLeading?.constant = _sidebarLeadingCurrentValue + translation.x
      
      // устанавливем прозрачность в зависимости от первоначального направления, а не через blurEffectView?.alpha = alpha > 0 ? alpha : 1 + alpha, чтобы не дергалась alpha
      if _directionPan! == .right {
        _sideBarBackgroundView?.alpha = translation.x / sideBarLeadingDef // уменьшаем прозрачность
      } else {
          _sideBarBackgroundView?.alpha = 1 + translation.x / sideBarLeadingDef// увеличиваем прозрачность
        }
    }
    
    if _sideBarLeading!.constant > sideBarLeadingVisible {
      _sideBarLeading?.constant = sideBarLeadingVisible
    }
    
    if gesture.state == .ended {
      // время анимации в зависимости от скорсти
      //var animationDuration = TimeInterval(CGFloat(60 / velocity.x).mod())
      
      var animationDuration           = cBaseViewController.SidePanel.fullAnimDur// время аанимации по умолчанию
      let secondAnimStartPos: CGFloat = view.frame.width * cBaseViewController.SidePanel.animPosStartMul// позиция, когда будет присвоено другое время аанимации
      
      if (_sideBarLeading!.constant.mod() <= secondAnimStartPos && velocity.x > 0) || (_sideBarLeading!.constant.mod() >= secondAnimStartPos && velocity.x < 0) {
        animationDuration = cBaseViewController.SidePanel.animDur
      }
      
      //let blurEffect = UIBlurEffect(style: .light)
      
      var performHideAnimation = true
      var rotateAngle: CGFloat = -.pi
      
      if velocity.x > 0 {
        performHideAnimation = false
        rotateAngle          = 2 * .pi
      }
      
      headerView?.performButtonRotateAnimation(rotationAngle: rotateAngle, animated: true)
      performSideMenuAnimation(animationDuration: animationDuration, hide: performHideAnimation, velocityX: velocity.x, completitionBlock: nil)
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Разрешить/запретить пан(т.к. пан всего один, такое решение сгодиться)
  func setPan(enabled: Bool) {
    guard let recgnizers = view.gestureRecognizers else {
      return
    }
    
    for gesture in recgnizers {
      if let pan = gesture as? UIPanGestureRecognizer {
        pan.isEnabled = enabled
      }
    }
  }
  //-----------------------------------------------------------------------------------

  //*** Анимация появления/исчезновение бокового меню
  func performSideMenuAnimation(animationDuration: TimeInterval, hide: Bool, velocityX: CGFloat, completitionBlock: (() -> Void)?) {
    let alphaMin: CGFloat              = 0
    let alphaMax: CGFloat              = 1
    let sideBarLeadingDef              = view.frame.width * cBaseViewController.SidePanel.widthScale// позиция по умолчаанию для констреинта левой стороны боковой панели
    let sideBarLeadingVisible: CGFloat = 0// максимальное значчение, когда видна боковая панель
    
    var sideBarConst = sideBarLeadingVisible
    var alpha        = alphaMax
    
    if hide {
      sideBarConst = -sideBarLeadingDef
      alpha        = alphaMin
    }
    
    UIView.animate(withDuration: animationDuration, animations: {
      self._sideBarLeading?.constant     = sideBarConst
      self._sideBarBackgroundView?.alpha = alpha
      
      self.view.layoutIfNeeded()
    }) { completition in
      completitionBlock?()
      
      self._handlePanXLocationInView   = velocityX > 0 ? sideBarLeadingDef : cBaseViewController.SidePanel.startHandlePan
      self._directionPan               = nil
      self._firsTapeXLocationInView    = nil
      self._sidebarLeadingCurrentValue = self._sideBarLeading!.constant
    }
  }
  //-----------------------------------------------------------------------------------
  
  // MARK: - Header view position
  
  //*** Изменение позиции хэдера в зависимости от скрола(данный метод вызываеться в scrollViewDidScroll())
  func setupHeaderPositionRelativeToScroll(scrollYOffset: CGFloat) {
    guard _scrollContentOfsetY != nil else {
      _scrollContentOfsetY = scrollYOffset
      
      return
    }
    
    let step             = scrollYOffset - _scrollContentOfsetY!
    let defHeaderViewTop = UIApplication.shared.statusBarFrame.height + cBaseViewController.Header.topConstant
    
    if _headerViewTopCurr == nil {
      _headerViewTopCurr = defHeaderViewTop
    }
    
    // если тянем вниз, а хэдер полностью виден, не дергаем лишний раз значение констреинта
    if (_headerViewTopCurr! - step) > defHeaderViewTop && _headerViewTop!.constant == defHeaderViewTop {
      return
    }
    
    _headerViewTop?.constant = _headerViewTopCurr! - step
    
    if _headerViewTop!.constant > defHeaderViewTop {
      _headerViewTop?.constant = defHeaderViewTop
    }
    
    if _headerViewTop!.constant < -headerView!.frame.height {
      _headerViewTop!.constant = -headerView!.frame.height
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Обновляем текущую позицию хэдера(вызывать в scrollViewDidEndDragging() и scrollViewDidEndDecelerating())
  func scrollEnded() {
    _scrollContentOfsetY = nil
    _headerViewTopCurr   = _headerViewTop?.constant
    
    view.layoutIfNeeded()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Приводим хэдер к дефолтной позиции(потянули вниз в начале скрола)
  func pullDownEnded(animated: Bool) {
    var duration = cBaseViewController.Header.showAnimDur
    
    if !animated {
      duration = 0
    }
    
    UIView.animate(withDuration: duration) {
      self._headerViewTop?.constant = UIApplication.shared.statusBarFrame.height + cBaseViewController.Header.topConstant
  
      self.view.layoutIfNeeded()
    }
    
    scrollEnded()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Скрыть хэдер
  func hideHeader() {
    _headerViewTop?.constant = -headerView!.frame.height
    
    view.layoutIfNeeded()
    scrollEnded()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Установка заголовка
  func setHeaderTittle(category: EnRequest) {
    headerView?.setTittle(category: category)
  }
  //-----------------------------------------------------------------------------------
}
