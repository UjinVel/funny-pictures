/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Главный экран, стартует первым
 */

import UIKit

class FPMainScreenPostsViewController: FPBaseViewController {
  // *** Views
  @IBOutlet weak var collectionViewPosts: UICollectionView!
  // *** Constraints
  @IBOutlet weak var collectionVIewPostsTop: NSLayoutConstraint!
  
  var presenter: FPMainScreenPoststPresenterProtocol!
  
  var postData: [FPSPostData]    = []// данные постов
  var categoriesData: FPSCatData = FPSCatData()// буфер для категорий(хэдер)
  var categoryType: EnRequest    = .funnyPictures// текущий тип категории
  
  let detailPostView: FPDetailPostView? = .getNibObject(fileName: FPDetailPostView.self)// детальное отображение поста
  
  private var _needToChangeCategory = false
  private let _reachability         = FPReachability()
		
// MARK: - UIViewController overrided
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    _reachability.startListen()
    initialSetup()
  }
  //-----------------------------------------------------------------------------------
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    // настройка отображения детального поста
    guard detailPostView != nil else {
      return
    }
    
    view.insertSubview(detailPostView!, belowSubview: super.headerView!)// добавляем под хэдер, здесь он точно существует
    detailPostView?.start()
    
    view.layoutIfNeeded()
  }
  //-----------------------------------------------------------------------------------
  
  override var prefersStatusBarHidden: Bool {
    return false
  }
  //-----------------------------------------------------------------------------------
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    collectionViewPosts.performBatchUpdates({
      self.collectionViewPosts.reloadData()
    }) { _ in
      self.detailPostView?.collectionViewDetailPost.performBatchUpdates({
        self.detailPostView?.collectionViewDetailPost.reloadData()
      }) { _ in
        for cell in self.detailPostView!.collectionViewDetailPost.visibleCells {
          cell.layoutSubviews()
        }
        
        self.detailPostView?.collectionViewDetailPost.scrollToItem(at: IndexPath(item: self.detailPostView!.currCellIndex, section: 0), at: .right, animated: true)
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - Initial setup
  
  //*** Начальна настройка элементов
  private func initialSetup() {
    presenter = FPMainScreenPostsPresenter(view: self)
    
    // _collectionViewPosts
    collectionViewPosts?.dataSource = self
    collectionViewPosts?.delegate   = self
    
    addRefreshControl()
    sideViewSetDelegate(self)
    
    FPSharedApplication.getInstance().currController = self
  }
  //-----------------------------------------------------------------------------------
  
  //*** Добавляем и настраиваем UIRefreshControl
  private func addRefreshControl() {
    let refreshControl             = UIRefreshControl()
    refreshControl.attributedTitle = NSAttributedString(string: cMainScreenPostsViewController.PullToRefresh.text)
    
    refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    collectionViewPosts.addSubview(refreshControl)
    
    let constants = cMainScreenPostsViewController.PullToRefresh.self
    
    // auto layout для refreshControl
    refreshControl.translatesAutoresizingMaskIntoConstraints = false
    // центр
    collectionViewPosts.addConstraint(NSLayoutConstraint(item: refreshControl,
                                                         attribute: .centerX,
                                                         relatedBy: .equal,
                                                         toItem: collectionViewPosts,
                                                         attribute: .centerX,
                                                         multiplier: constants.centerXMultiplier,
                                                         constant: constants.centerXConstant))
    let topConstant = cBaseViewController.Header.heightConstant - refreshControl.frame.height / 2// расстояние от верхней границы collectionViewPosts
    // верхняя граница
    collectionViewPosts.addConstraint(NSLayoutConstraint(item: refreshControl,
                                                         attribute: .top,
                                                         relatedBy: .equal,
                                                         toItem: collectionViewPosts,
                                                         attribute: .top,
                                                         multiplier: constants.topMultiplier,
                                                         constant: topConstant))
    // ширина
    refreshControl.addConstraint(NSLayoutConstraint(item: refreshControl,
                                                    attribute: .width,
                                                    relatedBy: .equal,
                                                    toItem: nil,
                                                    attribute: .width,
                                                    multiplier: constants.widthMultiplier,
                                                    constant: constants.widthConstant))
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - UIRefreshControl event callback
  
  //*** Обработка события обновления
  func refresh() {
    let refreshControls = collectionViewPosts.subviews.filter() { $0 as? UIRefreshControl != nil }
    
    for refreshControl in refreshControls {
      if postData.isEmpty {
        presenter.start()
      }
      
      collectionViewPosts.reloadData()
      (refreshControl as? UIRefreshControl)?.endRefreshing()
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - Caregory changed
  
  func changeCategory() {
    if presenter.isDataDownloadedAtThisMoment {
      _needToChangeCategory = true
      
      return
    }
    
    postData.removeAll()
    categoriesData.removeAll()
    
    presenter.start()
    
    collectionViewPosts.reloadData()
    
    for subView in view.subviews {
      if let reusableView = subView as? FPCollectionPostsReusableHeaderView {
        reusableView.collectionViewCategories.reloadData()
        
        break
      }
    }
  }
  //-----------------------------------------------------------------------------------

  func changeCategoryIfNeeded() -> Bool {
    if _needToChangeCategory {
      _needToChangeCategory = false
      
      changeCategory()
      
      return true
    }
    
    return _needToChangeCategory
  }
  //-----------------------------------------------------------------------------------
}
