/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Устанавливааем размер ячейки постов
 */

import UIKit

extension FPMainScreenPostsViewController: UICollectionViewDelegateFlowLayout {
  //*** Размер ячейки с учетом высоты картинки, надписей и кнопок
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    // for cells
    //let interitemSpasing = (_collectionViewPosts.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing
    // for lines
    guard let lineSpacing = (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing else {
      assert(false, "")
    }
    
    let cellWidth           = view.frame.width * cMainScreenPostsViewController.CollectionViewPosts.cellWidthScale - lineSpacing// ширина ячейки
    var size                = CGSize(width: 0, height: 0)// предварительный размер изображения
    var textHeight: CGFloat = 0// ширина надписи
    let constants           = cMainScreenPostsViewController.CollectionViewPosts.PostCell.self// размеры констреинтов ячейки
    
    if indexPath.row <= (postData.count - 1) {
      let imageTargetWidth = cellWidth - (constants.imageViewLeading + constants.imageViewTrailing)
      size                 = CGSize(width: imageTargetWidth, height: postData[indexPath.row].Image.height).getImageProportionalSize(sourceSize: CGSize(width: postData[indexPath.row].Image.width, height: postData[indexPath.row].Image.height))
      textHeight           = postData[indexPath.row].tittle.height(withConstrainedWidth: imageTargetWidth, font: UIFont.systemFont(ofSize: UIFont.systemFontSize))
      // сохраняем размер
      postData[indexPath.row].Image.aspectFitSize = size
    }
  
    let cellHeight = size.height + constants.tittleBottom + constants.tittleTop + constants.imageViewBottom + constants.buttonsContentViewHeight + textHeight// высота ячейки в зависимости от размера преобразованного изображения, высоты текста, отступов
    
    if collectionView == collectionViewPosts {
      return CGSize(width: cellWidth, height: cellHeight)
    } else {
        return CGSize(width: collectionView.frame.width - lineSpacing, height: collectionView.frame.height)
      }
  }
  //-----------------------------------------------------------------------------------
}
