/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

// MARK: - API

//*** GET - запросы на сервер
enum cApiGET {
  static let categories    = "https://funnyand.com/wp-json/wp/v2/categories/?sensetive_content="// ???
  static let posts         = "https://funnyand.com/wp-json/wp/v2/posts/?sensetive_content="// ???
  static let quotes        = "https://funnyand.com/wp-json/wp/v2/posts/?filter[category_name]=quotes&sensetive_content="// получение постов в категории quotes
  static let funnyPictures = "https://funnyand.com/wp-json/wp/v2/posts/?filter[category_name]=funny_pictures&sensetive_content="// получение постов в категории funny_pictures
  static let memes         = "https://funnyand.com/wp-json/wp/v2/posts/?filter[category_name]=memes_and_comics&sensetive_content="// получение постов в категории memes_and_comics
  static let random        = "https://funnyand.com/wp-json/wp/v2/posts/?random=1&sensetive_content="// получение постов в категории random
  static let search        = "https://funnyand.com/wp-json/wp/v2/posts/?search=%@&sensetive_content=false"// поиск
  static let media         = "https://funnyand.com/wp-json/wp/v2/media/"// запрос для данных картинки
  static let offset        = "&offset="// добавляеться к категориям
  
  static let reachableHost = "www.google.com"// адрес для проверки интернет соединения(NetworkReachabilityManager)
}
//-----------------------------------------------------------------------------------

//*** POST - запросы на сервер
enum cApiPOST {
  static let request = "https://funnyand.com/wp-admin/admin-ajax.php/"// url запроса для лайков
  // ключи и значения для составления массивов параметров для запроса(parametrs = ["action": "set_like", "like_action": "dislike", "post_id": postID])
  static let action     = "action"// ключь
  static let setLike    = "set_like"// значение action
  static let likeAction = "like_action"// ключь
  static let dislike    = "dislike"// значение likeAction
  static let like       = "like"// значение likeAction
  static let postID     = "post_id"// ключь
}
//-----------------------------------------------------------------------------------

// MARK: - special symbols

enum cSpecialSymbols {
  static let symbols = ["&#8217;": "'",// массив специальных символов, где ключь - спецсимвол, значение - его замена
                        "&#8230;": "...",
                        "&#8211;": "-",
                        "&#8212;": "-",
                        "&#8216;": "'",
                        "&amp;": "&",
                        "http": "https"]
  
  static let http  = "http"
  static let https = "https"
}
//-----------------------------------------------------------------------------------

// MARK: - JSON data formats

//*** Названия ключей, в json - обьекте, полученном с сервера
enum cJSONFormat {
  enum Post {
    static let id     = "id"
    static let media  = "featured_media"
    static let likes  = "likes_count"
    static let views  = "views_count"
    static let link   = "link"
    static let slug   = "slug"
    static let tittle = "title"
    
    enum Tittle {
      static let rendered = "rendered"
    }
  }
  
  enum Image {
    static let url     = "source_url"
    static let details = "media_details"
    
    enum MediaDetails {
      static let width  = "width"
      static let height = "height"
      static let file   = "file"
      static let sizes  = "sizes"
      static let meta   = "image_meta"
    }
  }
}
//-----------------------------------------------------------------------------------

// MARK: - ErrorHandling constants

enum cErrorHandling {
  enum Messages {
    static let alert                                = "Info"
    static let debug                                = "DEBUG"
    static let ok                                   = "OK"
    static let loadNibErr                           = "Can't load nib: "
    static let noInternet                           = "Looks like there's no internet connectivity. Check your connection and reload app."
    static let viewForSupplementaryElementOfKindErr = "Unexpected element kind"
    static let cantCastErr                          = "Can't cast"// невозможность преобразования типов
    static let cantGetNetworkReachabilityManager    = "Can't get NetworkReachabilityManager"
    static let NSRegularExpressionError             = "Can't get NSRegularExpressionError"
    static let formatError                          = "Server change response data format, request: "
    static let emptyPosts                           = "Request return 0 posts: "
    static let invalidImageURL                      = "Invalid link for download image"// строку невозможно преобразовать к URL
    static let cantPercentEncoding                  = "Can't allow percentEncoding to string"// строку невозможно экранировать
    static let noImage                              = "nil image in response"// ответ не выбил ошибку но изображения все равно нет
    static let noIndex                              = "There is no element that confirm to current pattern"// в массиве нет элемента, удовлетворяющего данным условиям сортировки
    static let noSuperview                          = "There is no superview for element"
    static let noParentVC                           = "There is no parent ViewController"// у элемента нет ViewController среди иерархии вьюх
    static let outOfRange                           = "Index out of range"// попытались обратиться к несуществующему элементу массива
    static let unexpectedElement                    = "Unexpected element"// неизвестная ссылка
  }
  
  enum Errors {
    
  }
}
//-----------------------------------------------------------------------------------


// MARK: - File types

enum cFile {
  enum Extensions {
    static let xib = ".xib"
  }
  
  enum FileType {
    static let nib = "nib"
  }
}
//-----------------------------------------------------------------------------------

// MARK: - BaseViewController

enum cBaseViewController {
  enum Header {
    static let trailingMultiplier: CGFloat = 1
    static let trailingConstant: CGFloat   = 0
    static let leadingMultiplier: CGFloat  = 1
    static let leadingConstant: CGFloat    = 0
    static let topMultiplier: CGFloat      = 1
    static let topConstant: CGFloat        = 0
    static let heightMultiplier: CGFloat   = 1
    static let heightConstant: CGFloat     = 80// высота хэдера
    
    static let showAnimDur: TimeInterval = 0.1// время анимации появления хэдера
    
    // названия категорий
    static let funny  = "Funny Pictures"
    static let memes  = "Memes"
    static let quotes = "Quotes"
    static let random = "Random"
  }
  
  //*** Боковая пнель с кнопками
  enum SidePanel {
    static let widthScale: CGFloat        = 0.6// множитель, показывающий во сколько раз ширина панели больше/меньше ширины экраана
    static let widthMultiplier: CGFloat   = 1// множитель для констреинта ширины
    static let leadingMultiplier: CGFloat = 1// множитель для констреинта левого бока
    static let leadingConstant: CGFloat   = 0// значение для констреинта левого бока
    static let topMultiplier: CGFloat     = 1// множитель для констреинта верхней границы
    static let topConstant: CGFloat       = 0// значение для констреинта верхней границы
    static let bottomMultiplier: CGFloat  = 1// множитель для констреинта нижней границы
    static let bottomConstant: CGFloat    = 0// значение для констреинта нижней границы
    
    static let minimumNumberOfTouches    = 1
    static let startHandlePan: CGFloat   = 50// когда SidePanel не видна, значение от левого края экрана, где начинает срабатывать пан
    static let fullAnimDur: TimeInterval = 0.2// время анимации появления/исчезновения SidePanel по умолчанию
    static let animDur: TimeInterval     = 0.5// время анимации появления/исчезновения SidePanel если довели до уровня view.width * animPosStartMul
    static let animPosStartMul: CGFloat  = 0.25// множитель для ширины экрана, для расчета позиции, когда присваивать другое время анимации(отличное от значения по умолчанию)
    
    //*** Вьюха, закрывающая видимое прстранство справа от боковой панели
    enum Background {
      static let widthMultiplier: CGFloat   = 2// множитель для констреинта ширины(значение констреинта - ширина экрана)
      static let leadingMultiplier: CGFloat = 1// множитель для констреинта левого бока(прикручиваем к правой границе SidePanel)
      static let leadingConstant: CGFloat   = 0// значение для констреинта левого бока
      static let topMultiplier: CGFloat     = 1// множитель для констреинта верхней границы
      static let topConstant: CGFloat       = 0// значение для констреинта верхней границы
      static let bottomMultiplier: CGFloat  = 1// множитель для констреинта нижней границы
      static let bottomConstant: CGFloat    = 0// значение для констреинта нижней границы
    }
  }
  
  //*** Вьюха по размеру статус-бара(если есть), чтобы хэдер заезжал под нее
  enum StatusBar {
    static let trailingMultiplier: CGFloat = 1
    static let trailingConstant: CGFloat   = 0
    static let leadingMultiplier: CGFloat  = 1
    static let leadingConstant: CGFloat    = 0
    static let topMultiplier: CGFloat      = 1
    static let topConstant: CGFloat        = 0
    static let heightMultiplier: CGFloat   = 1// значение высоты - размер статус-бара
  }
  
  static let heightMultiplier: CGFloat = 9// если contentSize.height у табличного элемента постов меньше его contentOffset.y + heightMultiplier * frame.height, инициируем занрузку новой порции
}
//-----------------------------------------------------------------------------------

// MARK: - ViewControllersIdentifiers

enum cViewControllersIdentifiers {
  static let mainScreenPostsViewController = "MainScreenPosts"// главный экран
  static let detailPostsViewController     = "DetailPostScreen"// пост детально
}
//-----------------------------------------------------------------------------------

// MARK: - Images name

enum cImages {
  static let placeholder =  "placeholder"
}
//-----------------------------------------------------------------------------------

// MARK: - MainScreenPostsViewController

enum cMainScreenPostsViewController {
  enum Reuseidentifiers {
    static let postsCell      = "postCell"
    static let categoriesCell = "categoriesCell"
    static let headerView     = "postsHeader"
    static let footerView     = "postsFooter"
    static let detailPostCell = "detailPostCell"
  }
  
  //*** Вьюха - футер с кнопками скачать, поделиться и т.д.
  enum PostFooter {
    static let trailingMultiplier: CGFloat = 1
    static let trailingConstant: CGFloat   = 0
    static let leadingMultiplier: CGFloat  = 1
    static let leadingConstant: CGFloat    = 0
    static let topMultiplier: CGFloat      = 1
    static let topConstant: CGFloat        = 0
    static let bottomMultiplier: CGFloat   = 1
    static let bottomConstant: CGFloat     = 0
    
    static let downloadAnimDuration: Int = 1000// время анимации загрузки картинки в миллисекундах, на которое увеличено реальное время загрузки(т.к. оно слишком мало)
  }
  
  //*** Вьюха для оповещения о результате лайков
  enum Vote {
    static let height: CGFloat            = 100
    static let width: CGFloat             = 250
    static let animDuration: TimeInterval = 0.5// время анимации появления и исчезновения вьюхи
    static let duration: Int              = 1000// время отображения вьюхи на экране в миллисекундах
    static let bottom: CGFloat            = 300// расстояние от нижней границы superview
    
    static let accepted     = "Your vote accepted"
    static let alreadyVoted = "You are already voted"
  }
  
  //*** Вьюха, для детального отображения поста
  enum DetailPost {
    static let trailingMultiplier: CGFloat = 1
    static let trailingConstant: CGFloat   = 0
    static let leadingMultiplier: CGFloat  = 1
    static let leadingConstant: CGFloat    = 0
    static let topMultiplier: CGFloat      = 1
    static let topConstant: CGFloat        = 0
    static let bottomMultiplier: CGFloat   = 1
    static let bottomConstant: CGFloat     = 0
    
    static let showHideAnimationDuration: TimeInterval    = 1// время аанимации появления/исчезновения вьюхи
    static let showHideFooterHeaderDuration: TimeInterval = 0.3// время анимации исчезновения/появления для футера и хэдера

    // надписи на хэдере ячейки
    static let like = " Like ∙ "
    static let view = "  View"

    //*** Ячейка, для детального отображения поста
    enum DetailPostCell {
      //*** Зум поста
      static let minimumZoomScale: CGFloat = 1// минимальное значение масштабируемости поста
      static let maximumZoomScale: CGFloat = 4// максимальное значение масштабируемости поста
      static let zoomScale: CGFloat        = 1// дэфолт
      //*** Гестуры
      static let zoomNumberOfTapsRequired: Int                = 2// количество тапов для срабаытвания события зума
      static let hideAnimMinimumPressDuration: CFTimeInterval = 0.5// время зажатия тапа для начала срабатывания анимации исчезновения вьюхи
      static let processPan: CGFloat                          = 10// промежуток translation -processPan - +processPan, когда начинает срабатывать обработка события пана
      static let processHideAnim: CGFloat                     = 50// промежуток -processHideAnim - +processHideAnim выйдя за пределы которого запускаеться анимация исчезновения вьюхи
      static let backToDefPos: TimeInterval                   = 0.3// время анимации возврата картинки в исходное положение во время обработки пана
      static let numberOfTapsRequired: Int                    = 1// количество тапов для обработки события начала анимации появления/исчезновения футера и хэдера
      static let numberOfTouchesRequired: Int                 = 2// количество пальцев -//-//
    }
  }
  
  //*** Обновление
  enum PullToRefresh {
    static let text = "Refreshing..."
    
    static let centerXMultiplier: CGFloat = 1// множитель позиции по оси x
    static let centerXConstant: CGFloat   = 0// значение позиции по оси x
    static let topMultiplier: CGFloat     = 1
    static let widthMultiplier: CGFloat   = 1
    static let widthConstant: CGFloat     = 200
  }
  
  //*** Надпись, когда нет данных постов
  enum NoDataLabel {
    static let text              = "No data is currently available. Please pull down to refresh."
    static let fontName          = "Palatino-Italic"
    static let fontSize: CGFloat = 20
    static let nuberOfLines      = 0
    static let xPos: CGFloat     = 0
    static let yPos: CGFloat     = 0
  }
  
  //*** Таблица постов
  enum CollectionViewPosts {
    static let cellWidthScale: CGFloat   = 1// зависит от ширины экрана
    static let cellHeightScale: CGFloat  = 0.8// зависит от высоты экрана
    static let cellCornerRadius: CGFloat = 10
    
    //*** Ячейка постов
    enum PostCell {
      static let tittleTop: CGFloat                = 15
      static let tittleBottom: CGFloat             = 15
      static let imageViewTrailing: CGFloat        = 10
      static let imageViewLeading: CGFloat         = 10
      static let imageViewBottom: CGFloat          = 15
      static let buttonsContentViewHeight: CGFloat = 60
    }
    
    //*** Таблица категорий
    enum CollectionViewCategories {
      static let categoriesOnScreen: CGFloat = 3// количесво категорий, показываемых на экране
      static let cellCornerRadius: CGFloat   = 10
    
      //*** Ячейка категорий
      enum CategoriesCell {
        static let height: CGFloat = 150
      }
    }
  }
}
//-----------------------------------------------------------------------------------
