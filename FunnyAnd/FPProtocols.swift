/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Протоколы описывающие взаимодействие основных элементов в MVP
 */

// MARK: - MainScreenPosts

// MARK: View interaction

import UIKit

//*** Обработка в представлении событий презентера, о поступлении данных из модели
protocol FPMainScreenPostsViewHandleUpdateProtocol: class {
  func didInitialDataDownloaded(categoriesBuff: FPSCatData, noInternet: Bool)// стартовая загрузка данных(посты+категории)
  func didDataDownloaded(data: [FPSPostData], noInternet: Bool)// загружена новая порция постов+картинок
  func didVoteSuccess(_ success: EnVoteSuccess, postIndex: Int)// запрос на лайк прошел успешно
}
//-----------------------------------------------------------------------------------

//*** Обработка запросов представления презентером
protocol FPMainScreenPoststPresenterProtocol: class {
  init(view: FPMainScreenPostsViewHandleUpdateProtocol)
  
  func start()// дать модели команду о начале стартовой загрузки(посты+категории)
  func downloadNewData(type: EnRequest)// загрузить следующую порцию постов
  func vote(like: EnVote, postID: int_fast64_t, postIndex: Int)// лайкнули пост
  
  var isDataDownloadedAtThisMoment: Bool { set get }// происходит ли загрузка данных в текущий момент
}
//-----------------------------------------------------------------------------------

// MARK: Model interaction

//*** Обработка презентером событий модели
protocol FPMainScreenPostsModelHandleUpdateProtocol: class {
  func didInitialDataDownloaded(categoriesBuff: FPSCatData, noInternet: Bool)// загружены данные(посты+картинки) при запуске приложения
  func didDataDownloaded(data: [FPSPostData], type: EnRequest, noInternet: Bool)// загружена новая порция постов+картинок
  func didVoteSuccess(_ success: EnVoteSuccess)// запрос на лайк прошел успешно
}
//-----------------------------------------------------------------------------------

//*** Обработка моделью запросов презентера
protocol FPMainScreenPoststModelProtocol {
  init(presenter: FPMainScreenPostsModelHandleUpdateProtocol)
  func start()// первоначальная загрузка данных(посты+категории)
  func downloadNewData(type: EnRequest)// загрузка новой порции постов
  func performVote(parametrs: [String: Any], like: EnVote)// лайк/дизлайк
}
//-----------------------------------------------------------------------------------
