/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Боковое меню(переключение категорий, поиск)
 */

import UIKit

//*** Протокол для отсеживания событий нажатия кнопок бокового меню
protocol SideViewDelegate: class {
  func sideViewFunnyPicturesDidTape()
  func sideViewMemesDidTape()
  func sideViewQuotesDidTape()
  func sideViewRandomDidTape()
}

class FPSideView: UIView {
  //*** Buttons
  @IBOutlet weak var funnyPicturesBtn: UIButton!
  @IBOutlet weak var memesBtn: UIButton!
  @IBOutlet weak var quotesBtn: UIButton!
  @IBOutlet weak var randomBtn: UIButton!
  
  weak var delegate: SideViewDelegate?
  
  private var _animationDuration  = cBaseViewController.SidePanel.fullAnimDur
  
// MARK: buttons actions
  
  //*** Нажали на кнопку бокового меню
  @IBAction func buttonAction(_ sender: UIButton) {
    let constants          = cBaseViewController.Header.self
    let velocityX: CGFloat = -1// ставим смещение отрицательным, т.к. вызываем анимацию исчезновения
    
    guard let baseCnt = getParentObject(FPBaseViewController.self) else {
      return
    }
    
    // убираем боковое меню
    baseCnt.performSideMenuAnimation(animationDuration: _animationDuration,
                                                  hide: true,
                                                  velocityX: velocityX,
                                                  completitionBlock: nil)
    baseCnt.headerView?.performButtonRotateAnimation(rotationAngle: -.pi, animated: true)// разворачиваем кнопку
    
    switch sender {
      case funnyPicturesBtn:
        baseCnt.headerView?.tittleLabel.text = constants.funny
        
        delegate?.sideViewFunnyPicturesDidTape()
      case memesBtn:
        baseCnt.headerView?.tittleLabel.text = constants.memes
        
        delegate?.sideViewMemesDidTape()
      case randomBtn:
        baseCnt.headerView?.tittleLabel.text = constants.random
        
        delegate?.sideViewRandomDidTape()
      case quotesBtn:
        baseCnt.headerView?.tittleLabel.text = constants.quotes
        
        delegate?.sideViewQuotesDidTape()
      default:
        assert(false, cErrorHandling.Messages.unexpectedElement)
    }
  }
  //-----------------------------------------------------------------------------------
}
