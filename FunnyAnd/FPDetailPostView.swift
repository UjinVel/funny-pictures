/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Вьюха для детального отображения поста
 */

import UIKit

class FPDetailPostView: UIView {
  //*** Views
  @IBOutlet weak var collectionViewDetailPost: UICollectionView!
  @IBOutlet weak var footerView: UIView!
  @IBOutlet weak var headerView: UIView!
  //*** Labels
  @IBOutlet weak var tittleLabel: UILabel!
  @IBOutlet weak var infoLabel: UILabel!// отображает количество просмотров и комментариев
  //*** Constraints
  @IBOutlet weak var collectionViewTop: NSLayoutConstraint!
  @IBOutlet weak var headerTop: NSLayoutConstraint!
  @IBOutlet weak var footerBottom: NSLayoutConstraint!
  
  var currCellIndex: Int = 0// индекс текущей ячейки
  weak var parentViewController: FPMainScreenPostsViewController? { return getParentObject(FPMainScreenPostsViewController.self) }// родительский вью котроллер
  
  var isHeaderAndFooterOnScreen: Bool {// видимы ли хэдер и футер
    if headerTop.constant != 0 {
      return false
    }
    
    return true
  }
 
// MARK: - Initial setups
  
  func start() {
    setupPositions()
    setupCollectionView()
    setupFooter()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Настраиваем позицию с Auto Layout
  private func setupPositions() {
    guard let superView = superview else {
      assert(false, cErrorHandling.Messages.noSuperview)
    }
    
    let constants = cMainScreenPostsViewController.DetailPost.self
    
    translatesAutoresizingMaskIntoConstraints = false
    
    // верх
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .top,
                                               multiplier: constants.topMultiplier,
                                               constant: UIApplication.shared.statusBarFrame.height + constants.topConstant))
    // лево
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .leading,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .leading,
                                               multiplier: constants.leadingMultiplier,
                                               constant: constants.leadingConstant))
    // право
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .trailing,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .trailing,
                                               multiplier: constants.trailingMultiplier,
                                               constant: constants.trailingConstant))
    // низ
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .bottom,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .bottom,
                                               multiplier: constants.bottomMultiplier,
                                               constant: constants.bottomConstant))
    
    isHidden = true
    
    headerTop.constant    = -headerView.frame.height
    footerBottom.constant = footerView.frame.height
  }
  //-----------------------------------------------------------------------------------
  
  //*** Настройка футера
  private func setupFooter() {
    guard let footer = UIView.getNibObject(fileName: FPPostFooter.self) else {
      return
    }
    
    footer.setupPosition(superView: footerView)
    
    footer.delegate = self
  }
  //-----------------------------------------------------------------------------------
  
  //*** Настройка таблицы
  private func setupCollectionView() {
    collectionViewDetailPost.dataSource = parentViewController
    collectionViewDetailPost.delegate   = parentViewController
    
    // регестрируем ячейку
    collectionViewDetailPost.register(UINib(nibName: String(describing: FPDetailPostCell.self), bundle: nil), forCellWithReuseIdentifier: cMainScreenPostsViewController.Reuseidentifiers.detailPostCell)
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - collectionViewDetailPost
  
  //*** Установка данных текущей ячейки
  func setCurrCellData() {
    let constants = cMainScreenPostsViewController.DetailPost.self
    currCellIndex = lroundf(Float(collectionViewDetailPost.contentOffset.x) / Float(frame.width))// oбноление текущего индекса ячейки
    
    guard let data = parentViewController?.postData else {
      return
    }
    
    collectionViewDetailPost.isScrollEnabled = true
    
    // обновляем надписи
    tittleLabel.text = data[currCellIndex].tittle
    infoLabel.text   = data[currCellIndex].likesCount.description + constants.like + data[currCellIndex].viewsCount.description + constants.view
  }
  //-----------------------------------------------------------------------------------
  
  //*** Установка пана(разрешен или нет) во всех видимых ячейках
  func setPanGesturesInVisibleCells(enabled: Bool) {
    for cell in collectionViewDetailPost.visibleCells {
      if let cell_ = cell as? FPDetailPostCell {
        cell_.setPan(enabled: enabled)
      }
    }
  }
  //-----------------------------------------------------------------------------------
}
