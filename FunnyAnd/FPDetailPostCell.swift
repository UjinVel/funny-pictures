/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Ячейка для детального отображения поста
 */

import UIKit

class FPDetailPostCell: UICollectionViewCell {
  //*** Views
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var scrollContentView: UIView!
  //*** Constraints
  @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
  @IBOutlet weak var imageViewYPos: NSLayoutConstraint!
  
  private var _postData: FPSPostData?
  private var _indexPath: IndexPath?
  private var _isHideAnimationOver: Bool      = true// признак того, что анимация исчезновения вьюхи с таблицей детального поста завершена(нужен, чтобы в момент анимации не срабатывало событе LongPress)
  
  var _imageViewYPos: CGFloat = 0
  var _constants              = cMainScreenPostsViewController.DetailPost.DetailPostCell.self// константные значения ячейки
  
// MARK: - UICollectionViewCell overrided
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    initialSetup()
  }
  //-----------------------------------------------------------------------------------
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    setImagePosition()
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - Cell setup
  
  //*** Начальна установка ячейки
  private func initialSetup() {
    addTapeGesture()
    addLongPressGesture()
    addDoubleTapeGesture()
    addPanGesture()
    
    // настройка зума
    scrollView.minimumZoomScale = _constants.minimumZoomScale
    scrollView.maximumZoomScale = _constants.maximumZoomScale
    scrollView.zoomScale        = _constants.zoomScale
  }
  //-----------------------------------------------------------------------------------
  
  //*** Установка размера(по высоте) и позиции картинки в зависимости от оригинального размера
  private func setImagePosition() {
    let contentSize = updateScrollContentSize()
    
    scrollView.delegate      = self
    imageViewHeight.constant = contentSize.height// ширина imageView по ширене изображения
    
    // центрируем изображение, если нужно
    var yPos: CGFloat = 0
    
    if contentSize.height > frame.height {
      yPos = (contentSize.height - frame.height) / 2
    }
    
    imageViewYPos.constant = yPos
    
    setPan(enabled: true)
  }
  //-----------------------------------------------------------------------------------
  
  //*** Установка ContentSize() у скрола ячейки
  @discardableResult private func updateScrollContentSize() -> CGSize {
    guard let postData = _postData else {
      return CGSize(width: 0, height: 0)
    }
    
    let contentSize = CGSize(width: frame.width, height: postData.Image.height).getImageProportionalSize(sourceSize: CGSize(width: postData.Image.width, height: postData.Image.height))// получили предварительный размер изображения после Aspect Fit
    
    scrollView.contentSize = contentSize// contentSize у скрола по размеру изображения
    
    return contentSize
  }
  //-----------------------------------------------------------------------------------
  
  //*** Инициируем загрузку изображения или получчение его из кэша
  func setup(data: FPSPostData, indexPath: IndexPath) {
    // загружаем или получаем из кэша изображение(нарушение MVP)
    imageView.af_setImage(withURLRequest: URLRequest(url: data.Image.url!), placeholderImage: UIImage(named: cImages.placeholder))
    
    _postData  = data
    _indexPath = indexPath
    
    imageView.isHidden = false
    
    setImagePosition()
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - Gestrure
  
  //*** Гестура для запуска анимации исчезновения FPDetailPostView через долгое нажатие
  private func addLongPressGesture() {
    let pressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressEvent(gesture:)))
    
    pressGesture.minimumPressDuration = _constants.hideAnimMinimumPressDuration
    pressGesture.delegate             = self
    
    addGestureRecognizer(pressGesture)
  }
  //-----------------------------------------------------------------------------------
  
  func handleLongPressEvent(gesture: UILongPressGestureRecognizer) {
    if _isHideAnimationOver && !getParentObject(FPDetailPostView.self)!.isHidden && scrollView.zoomScale == _constants.zoomScale {
      hide()
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Гестура для запуска анимации исчезновения FPDetailPostView через смахивание
  private func addPanGesture() {
    let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(gesture:)))
    
    panGesture.delegate = self
    
    addGestureRecognizer(panGesture)
  }
  //-----------------------------------------------------------------------------------
  
  func handlePan(gesture: UIPanGestureRecognizer) {
    let translation = gesture.translation(in: self)
    
    guard let parentCnt = getParentObject(FPDetailPostView.self) else {
      return
    }
    
    switch gesture.state {
    case .began:
      _imageViewYPos = imageViewYPos.constant
    case .ended:
      
// TODO: исправить
      
      // оборачиваем текущую позицию вьюхи в блок UIView.animate для корректной обработки auto layout
      UIView.animate(withDuration: 0, animations: {
        self.imageViewYPos.constant = self._imageViewYPos + translation.y
        
        self.layoutIfNeeded()
      }) { completition in
        parentCnt.collectionViewDetailPost.isScrollEnabled = true
        
        if translation.y >= self._constants.processHideAnim || translation.y <= -self._constants.processHideAnim {
          // запускаем анимацию исчезновения
          self.hide()
        } else {
            // возвращаем картинку в исходное положение
            UIView.animate(withDuration: self._constants.backToDefPos) {
              self.imageViewYPos.constant = self._imageViewYPos
            
              self.layoutIfNeeded()
            }
          
            parentCnt.performShowHideFooterAndHeaderAnim()// здесь хэдер и футер точно не видны
        }
      }
    default:
      if translation.y < -_constants.processPan || translation.y > _constants.processPan {// когда обрабатываем пан, скролить на следующий пост нельзя
        parentCnt.collectionViewDetailPost.isScrollEnabled = false
      }
      
      if !parentCnt.collectionViewDetailPost.isScrollEnabled {
        imageViewYPos.constant = _imageViewYPos + translation.y
        
        // если начали смахивать и хэдер с футером на экране
        if (translation.y >= self._constants.processHideAnim || translation.y <= -self._constants.processHideAnim) && parentCnt.isHeaderAndFooterOnScreen {
          parentCnt.performHideFooterAndHeaderAnim(animated: true)
        }
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Разрешить/запретить пан(т.к. пан всего один, такое решение сгодиться)
  func setPan(enabled: Bool) {
    guard let recgnizers = gestureRecognizers else {
      return
    }
    
    for gesture in recgnizers {
      if let pan = gesture as? UIPanGestureRecognizer {
        pan.isEnabled = enabled
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Выполняем анимацию исчезновения FPDetailPostView
  private func hide() {
    _isHideAnimationOver = false
    
    // начально положение - положение imageView, кроме высоты статус бара, т.к. анимироваанную вьюху добавляем на главную вьюху контроллера и смещение у скрола, если есть
    let x      = imageView.frame.origin.x
    let y      = UIApplication.shared.statusBarFrame.height + imageView.frame.origin.y - scrollView.contentOffset.y
    let width  = imageView.frame.width
    let height = imageView.frame.height
    
    let startFrame = CGRect(x: x, y: y, width: width, height: height)
    
    getParentObject(FPDetailPostView.self)?.performHideAnimation(indexPath: _indexPath!, startFrame: startFrame) {
      self._isHideAnimationOver = true
      
      UIView.animate(withDuration: 0) {
        self.imageViewYPos.constant = self._imageViewYPos
        
        self.layoutIfNeeded()
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Гестура для зума/возврата в исходное значение
  private func addDoubleTapeGesture() {
    let doubleTapeGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapeEvent(gesture:)))
    
    doubleTapeGesture.numberOfTapsRequired = _constants.zoomNumberOfTapsRequired
    doubleTapeGesture.delegate = self
    
    addGestureRecognizer(doubleTapeGesture)
  }
  //-----------------------------------------------------------------------------------
  
  func handleDoubleTapeEvent(gesture: UITapGestureRecognizer) {
    if scrollView.zoomScale != _constants.zoomScale {// если изображение уже увеличено, возвращаем в исходное значение
      scrollView.setZoomScale(_constants.zoomScale, animated: true)
      updateScrollContentSize()//
    } else {// увеличиваем изображение
        let location       = gesture.location(in: self)
        let zoomRectHeight = contentView.frame.height / _constants.maximumZoomScale
        let zoomRectWidth  = contentView.frame.width / _constants.maximumZoomScale
        let zoomRectX      = location.x - zoomRectWidth / 2
        let zoomRectY      = location.y - zoomRectHeight / 2
      
        scrollView.zoom(to: CGRect(x: zoomRectX, y: zoomRectY, width: zoomRectWidth, height: zoomRectHeight), animated: true)
      }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Гестура для появления/исчезновения футера и хэдера
  private func addTapeGesture() {
    let tapeGesture = UITapGestureRecognizer(target: self, action: #selector(handleTape(gesture:)))
    
    tapeGesture.numberOfTapsRequired    = _constants.numberOfTapsRequired
    tapeGesture.numberOfTouchesRequired = _constants.numberOfTouchesRequired
    
    addGestureRecognizer(tapeGesture)
  }
  //-----------------------------------------------------------------------------------
  
  func handleTape(gesture: UITapGestureRecognizer) {
    guard let parentView = getParentObject(FPDetailPostView.self) else {
      return
    }
    
    parentView.performShowHideFooterAndHeaderAnim()
  }
  //-----------------------------------------------------------------------------------
}
