/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

class FPCollectionPostsReusableHeaderView: UICollectionReusableView {
  //*** Views
  @IBOutlet weak var collectionViewCategories: UICollectionView!
  //*** Constraints
  @IBOutlet weak var collectionViewCategoriesTop: NSLayoutConstraint!
  
  var categoriesData: FPSCatData = FPSCatData()
  
  func initialSetup(data: FPSCatData) {
    collectionViewCategoriesTop.constant = cBaseViewController.Header.heightConstant
    
    categoriesData = data
    
    collectionViewCategories?.delegate   = self
    collectionViewCategories?.dataSource = self
    
    //setupCollectionViewCellsize()
    collectionViewCategories.reloadData()
  }
  //-----------------------------------------------------------------------------------
  
  func setupCollectionViewCellsize() {
    let lineSpacing = (collectionViewCategories.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing
    let constants   = cMainScreenPostsViewController.CollectionViewPosts.CollectionViewCategories.self
    
    if lineSpacing != nil && collectionViewCategories.superview != nil {
      // устаановка размера ячейки
      (collectionViewCategories.collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize = CGSize(width: (collectionViewCategories.superview!.frame.width - (constants.categoriesOnScreen - 1) * lineSpacing!) / constants.categoriesOnScreen, height: constants.CategoriesCell.height)
      
      collectionViewCategories.collectionViewLayout.invalidateLayout()
    }
  }
  //-----------------------------------------------------------------------------------
}
