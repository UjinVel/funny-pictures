/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Запросы на получение данных(разбор json-а возможно лучше вынести в презентер)
 */

import AlamofireImage
import Alamofire

enum EnJsonFormat: String {
  case formatOK
  case formatERROR// общий ответ для ошибки данных
  case widthFormatError// ширину картинки невозможно преобразовать к Float
  case heightFormatError// высоту картинки невозможно преобразовать к Float
  case idFormatError// id поста невозможно преобразовать к Int
  case featuredMediaFormatError// featuredMedia поста невозможно преобразовать к Int
  case likesFormatError// количество лайков поста невозможно преобразовать к Int
  case viewsFormatError// количество просмотров поста невозможно преобразовать к Int
  case JSONSerialization_ERROR
  case like_success
  case already_liked
}

class FPNetworkQueries {

// MARK: - JSON error check
  
  //*** Проверка ответа после Alamofire.request(api!).responseJSON
  private static func errorCheck(_ result: Result<Any>) ->Any? {
    switch result {
      case .failure(let error):
        if let err = error as? URLError, err.code == .notConnectedToInternet || err.code == .networkConnectionLost || err.code == .cannotFindHost
        || err.code == .cannotConnectToHost {
          return nil// если нет интернета, возвращаем nil
        } else {
            assert(false, error.localizedDescription)
          }
      case .success(let json):
        return json
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - parse JSON
  
  //*** Разбор ответа о данных постов
  private static func parseJSONPostsData(_ json: Any, category: EnRequest) -> ([FPSPostData], EnJsonFormat) {
    var format: EnJsonFormat  = .formatOK
    var result: [FPSPostData] = []// результат ввиде массива структур
    var currentPost           = FPSPostData()// структура для текущего поста
  
    // получение ответа для поста
    if let jsonArr = json as? [Any], !jsonArr.isEmpty {
      // ответ пришел ввиде массива
      for currObj in jsonArr {
        if let currDict = currObj as? [String: AnyObject], !currDict.isEmpty, format == .formatOK {
          // элемент массива - это ассоциативный массив
          for (key, value) in currDict {
            // формируем структуру SCurrentPost
            switch key {
              case cJSONFormat.Post.id:
                if let id = value as? Int {
                  currentPost.id = int_fast64_t(id)
                } else {
                    format = .idFormatError
                  }
              case cJSONFormat.Post.media:
                if let featuredMedia = value as? Int {
                  currentPost.featuredMedia = int_fast64_t(featuredMedia)
                } else {
                    format = .featuredMediaFormatError
                  }
              case cJSONFormat.Post.likes:
                if let likesCount = value as? Int {
                  currentPost.likesCount = int_fast64_t(likesCount)
                } else {
                    format = .likesFormatError
                  }
              case cJSONFormat.Post.views:
                if let viewsCount = value as? Int {
                  currentPost.viewsCount = int_fast64_t(viewsCount)
                } else {
                    format = .viewsFormatError
                  }
              case cJSONFormat.Post.link:
                if let link = value as? String {
                  currentPost.link = link
                }
              case cJSONFormat.Post.slug:
                if let slug = value as? String {
                  currentPost.name = slug
                }
              case cJSONFormat.Post.tittle:
                if let titleDict = value as? [String: String], let renderedText = titleDict[cJSONFormat.Post.Tittle.rendered] {
                  // строка по ключу rendered сущесвует
                  currentPost.tittle = renderedText
                } else {
                    format = .formatERROR
                  }
              default:// не все ключи проверяються, поэтому по умолчанию .ok
                format = .formatOK
            }
          }
          
          if !currentPost.isPostDataOk() {
            // данные не заполненны
            format = .formatERROR
            
            break
          }
    
          currentPost.category = category
          
          result.append(currentPost)// добавляем данные поста в конец массива
          currentPost.clear()// очищаем структуру данных
        } else {
            format = .formatERROR
          
            break
          }
      }
    } else {
        format = .formatERROR
      }
    
    return (result, format)
  }
  //-----------------------------------------------------------------------------------

  //*** Разбор ответа о данных изображения
  private static func parseJSONImageData(_ json: Any) -> (FPSImageData, EnJsonFormat) {
    var format: EnJsonFormat = .formatOK
    var imageData            = FPSImageData()
  
    switch json {
      case let dict as [String: AnyObject]:
        for (key, value) in dict {
          switch key {
            case cJSONFormat.Image.details:
              if let mediaDict = value as? [String: AnyObject] {
                for (mediaKey, mediaValue) in mediaDict {
                  switch mediaKey {
                    case cJSONFormat.Image.MediaDetails.width:
                      if let width = mediaValue.floatValue {
                        imageData.width = CGFloat(width)
                      } else {
                          format = .widthFormatError
                        }
                    case cJSONFormat.Image.MediaDetails.height:
                      if let height = mediaValue.floatValue {
                        imageData.height = CGFloat(height)
                      } else {
                          format = .heightFormatError
                        }
                    default:// не все ключи проверяються
                      format = .formatOK
                  }
                }
              }
            case cJSONFormat.Image.url:
              if let url = value as? String {
                imageData.urlStr = url
              } else {
                  format = .formatERROR
                }
            default:
              format = .formatOK
          }
        }
      default:
        format = .formatERROR
    }
    
    if !imageData.isImageDataOk() {
      format = .formatERROR
    }
    
    return (imageData, format)
  }
  //-----------------------------------------------------------------------------------

  //*** Разбор ответа об успешности лайка(бэкэнд работает хреново!!!!)
  private static func parsePOSTLikes(_ json: Any) -> (EnJsonFormat){
    var format: EnJsonFormat = .formatOK
    
    switch json {
      case _ as [String: [String: Int]]:// уже лайкали
        format = .already_liked
        //print(json)
      case _ as [String: Int]:// лайкнули
        format = .like_success
        //print(json)
      default:
        format = .like_success
        //print(json)
    }
    
    return format
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - new data requests
  
  //*** Получение новых постов
  static func getNewPostData(offset: integer_t, funnyRequest: EnRequest, callbackClosure: @escaping (_ catBuff: [FPSPostData]?, _ funnyRequest: String) -> Void) {
    var api: String?
    
    switch funnyRequest {
      case .funnyPictures:
        api = cApiGET.funnyPictures + FPSharedApplication.getInstance().sensitiveContent().description + cApiGET.offset + offset.description
      case .memes:
        api = cApiGET.memes + FPSharedApplication.getInstance().sensitiveContent().description + cApiGET.offset + offset.description
      case .quotes:
        api = cApiGET.quotes + FPSharedApplication.getInstance().sensitiveContent().description + cApiGET.offset + offset.description
      case .random:
        api = cApiGET.random + FPSharedApplication.getInstance().sensitiveContent().description + cApiGET.offset + offset.description
    }
    
    Alamofire.request(api!).responseJSON { dataJSON in// api здесь точно не пустой
      DispatchQueue.global(qos: .default).async {
        var res_: [FPSPostData]?
        var format_: EnJsonFormat = .formatOK
        
        if let json = errorCheck(dataJSON.result) {
          let (res, format) = parseJSONPostsData(json as Any, category: funnyRequest)// parseJSONPostsData(dataJSON.result.value as Any, category: funnyRequest)
          
          format_ = format
          res_    = res
        }
      
        if format_ == .formatOK {
          DispatchQueue.main.async {
            callbackClosure(res_, api!)
          }
        } else {
            assert(false, cErrorHandling.Messages.formatError)
          }
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Получение данных изображения
  static func getImage(featuredMedia: int_fast64_t, callbackClosure: @escaping (_ imageData: FPSImageData?) -> Void) -> Void {
    Alamofire.request(cApiGET.media + featuredMedia.description).responseJSON { dataJSON in
      DispatchQueue.global(qos: .default).async {
        var res_: FPSImageData?
        var format_: EnJsonFormat = .formatOK
        
        if let json = errorCheck(dataJSON.result) {
          let (res, format) = parseJSONImageData(json as Any)
          
          format_ = format
          res_    = res
        }
        
        if format_ == .formatOK {
          DispatchQueue.main.async {
            callbackClosure(res_)
          }
        } else {
            assert(false, cErrorHandling.Messages.formatError + cApiGET.media + featuredMedia.description)
          }
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - POST requests
  
  //*** POST - запрос на лайк, дизлайк
  static func performPOST(parametrs: [String: Any], callbackClosure: @escaping (_ success: EnJsonFormat) -> Void) {
    Alamofire.request(cApiPOST.request, method: .post, parameters: parametrs, encoding: URLEncoding.default, headers: nil).responseJSON { dataJSON in
      DispatchQueue.global(qos: .default).async {
        guard let json = errorCheck(dataJSON.result) else {
          return
        }
        
        let res = parsePOSTLikes(json as Any)
        
        if res != .formatERROR {
          DispatchQueue.main.async {
            callbackClosure(res)
          }
        } else {
            assert(false, cErrorHandling.Messages.formatError + parametrs.description)
          }
      }
    }
  }
  //-----------------------------------------------------------------------------------
}
