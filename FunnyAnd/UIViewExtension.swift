/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

extension UIView {
  //*** Загрузка обьекта из xib-a(имя класса должно совпадать с именем .xib файла)
  static func getNibObject<T>(fileName: T.Type) -> T? {
    guard Bundle.main.path(forResource: String(describing: fileName), ofType: cFile.FileType.nib) != nil else {// проверяем, существует ли файл
      assert(false, cErrorHandling.Messages.loadNibErr + String(describing: fileName) + cFile.Extensions.xib)
    }
    
    return Bundle.main.loadNibNamed(String(describing: fileName), owner: nil, options: nil)!.first as? T
  }
  //-----------------------------------------------------------------------------------
  
  //*** Получение ссылки на родительский элемент, если такой есть
  func getParentObject<T>(_ type: T.Type) -> T? {
    var parentResponder: UIResponder? = self
    
    while parentResponder != nil {
      parentResponder = parentResponder!.next
      if let parentObj = parentResponder as? T {
        return parentObj
      }
    }
    
    return nil
  }
  //-----------------------------------------------------------------------------------
  
  //*** Получение координат элемента в системе координат родителя(если такой есть)
  @discardableResult func getCoordinatesInView(_ parentView: UIView) -> CGRect? {
    var parentResponder: UIResponder? = self
    var x: CGFloat                    = frame.origin.x
    var y: CGFloat                    = frame.origin.y
    
    var contentOffset: CGPoint?
    
    while parentResponder != nil {
      parentResponder = parentResponder!.next
      
      if let collView = parentResponder as? UICollectionView {
        contentOffset = collView.contentOffset
      }
      
      if let view_ = parentResponder as? UIView {
        if view_ == parentView {
          if contentOffset != nil {
            y = y - contentOffset!.y
            x = x - contentOffset!.x
          }
          
          return CGRect(x: x, y: y, width: frame.width, height: frame.height)
        } else {
            x += view_.frame.origin.x
            y += view_.frame.origin.y
          }
      }
    }
    
    return nil
  }
  //-----------------------------------------------------------------------------------
}
