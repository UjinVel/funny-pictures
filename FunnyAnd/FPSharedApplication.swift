/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Одиночка(кастомный ImageDownloader и отображение сообщения об отсутствии интернета)
 * Useful links: https://krakendev.io/blog/the-right-way-to-write-a-singleton, https://developer.apple.com/swift/blog/?id=7
 */

import AlamofireImage

class FPSharedApplication {
  private static let _sharedInstance = FPSharedApplication()// инициализируя константу глобально или глобально для класса - делаем ее потоково безопасной(dispatch_once вызываеться автоматически, в стеке вызова фигурирует dispatch_once_f)
  private let _syncQueue             = DispatchQueue.global(qos: .default)// очередь для потоковобезопасного доступа
  private var _sensitiveContent      = false// фильтрация контента
  private let _imageCache            = AutoPurgingImageCache(memoryCapacity: 100_000_000, preferredMemoryUsageAfterPurge: 60_000_000)
  
  lazy var imageDownloader = FPSharedApplication.getInstance().setupImageDownloader()// кастомный ImageDownloader
  
  weak var currController: UIViewController?// ссылка на текущий вью контроллер(для вывода сообщений об отсутствии интернета)
  
  private var _alert = UIAlertController()
  
  private init() {}// запрещаем другим обьектам создавать экземпляры одиночки
  
  static func getInstance() -> FPSharedApplication {
    return ._sharedInstance
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - filtration
  
  //*** Установка флага о фильтрации контента
  func setSensitiveContent(_ content: Bool) {
     // барьерная запись(очередь ожидает завершения всех ее блоков, далее выполняет барьерный блок, потом снова возможно параллельное чтение)
    _syncQueue.async(flags: .barrier) { [weak self] in
      guard let strongSelf = self else {
        return
      }
      
      strongSelf._sensitiveContent = content
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Получение значения флага о фильтрации контента
  func sensitiveContent() -> Bool {
    var lockalSensitiveContent = false
    
    // разрешаем параллельное чтение из разных потоков
    _syncQueue.sync { [weak self] in
      guard let strongSelf = self else {
        return
      }
      
      lockalSensitiveContent = strongSelf._sensitiveContent
    }
    
    return lockalSensitiveContent
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - image cache and download
  
  //*** Инициализация кастомного ImageDownloader
  private func setupImageDownloader() -> ImageDownloader {
    return ImageDownloader(
      configuration: ImageDownloader.defaultURLSessionConfiguration(),
      downloadPrioritization: .fifo,
      maximumActiveDownloads: 4,
      imageCache: _imageCache
    )
  }
  //-----------------------------------------------------------------------------------
  
  //*** Получение изображения из кэша
  func getImageFromCache(url: URL) -> UIImage? {
    var lockalCachedImage: UIImage? = nil
    
    _syncQueue.sync { [weak self] in
      guard let strongSelf = self else {
        return
      }
      
      lockalCachedImage = strongSelf._imageCache.image(for: URLRequest(url: url))
    }
    
    return lockalCachedImage
  }
  //-----------------------------------------------------------------------------------
  
  //*** Добавление изображения в кэш, если изображение не добавилось автоматически
  func addImageToCache(url: URL, image: UIImage) {
    _syncQueue.async(flags: .barrier) { [weak self] in
      guard let strongSelf = self else {
        return
      }
      
      if strongSelf._imageCache.image(for: URLRequest(url: url)) == nil {
        strongSelf._imageCache.add(image, for: URLRequest(url: url))
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Очистка кэша изображений
  func clearImageCache() {
    _syncQueue.async(flags: .barrier) { [weak self] in
      guard let strongSelf = self else {
        return
      }
      
      strongSelf._imageCache.removeAllImages()
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Загрузка изображения
  func downloadImage(url: URL, dispatchGroup: DispatchGroup?, callbackClosure: @escaping (_ image: UIImage) -> Void) {
      if let image = self.getImageFromCache(url: url) {
        callbackClosure(image)
        dispatchGroup?.leave()
      } else {
          self.imageDownloader.download(URLRequest(url: url)) { responseImage in
            defer { dispatchGroup?.leave() }// выходим из группы диспетчеризации
        
            guard responseImage.error == nil else {
              assert(false, responseImage.error!.localizedDescription)
            }
        
            guard let image_ = responseImage.result.value else {
              assert(false, cErrorHandling.Messages.noImage)
            }
        
            self.addImageToCache(url: url, image: image_)// кэшируем изображение(при вызове download() кэшируеться автоматически, но на всякий случай проверяем)
            callbackClosure(image_)
          }
        }
  }
  //-----------------------------------------------------------------------------------
}
