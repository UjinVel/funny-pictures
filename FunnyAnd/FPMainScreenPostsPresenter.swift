/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Обработка запросов представления и передача их модели
 */

import Foundation

class FPMainScreenPostsPresenter: FPMainScreenPoststPresenterProtocol {
  weak var view: FPMainScreenPostsViewHandleUpdateProtocol?
  var model: FPMainScreenPoststModelProtocol!
  
  var isDataDownloadedAtThisMoment = false
  
  var currPostIndex: Int = 0
  
  required init(view: FPMainScreenPostsViewHandleUpdateProtocol) {
    isDataDownloadedAtThisMoment = true
    self.view                    = view
    model                        = FPMainScreenPostsModel(presenter: self)
  }
  //-----------------------------------------------------------------------------------
  
  //*** Начальная загрузка данных
  func start() {
    isDataDownloadedAtThisMoment = true
    
    model.start()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Инициировать загрузку новой порции постов
  func downloadNewData(type: EnRequest) {
    isDataDownloadedAtThisMoment = true
    
    model.downloadNewData(type: type)
  }
  //-----------------------------------------------------------------------------------
  
  //*** Лайкнули пост
  func vote(like: EnVote, postID: int_fast64_t, postIndex: Int) {
    isDataDownloadedAtThisMoment = true
    currPostIndex                = postIndex
    
    var parametrs: [String: Any]?

    switch like {
      case .down:
        parametrs = [cApiPOST.action: cApiPOST.setLike, cApiPOST.likeAction: cApiPOST.dislike, cApiPOST.postID: postID]
      case .up:
        parametrs = [cApiPOST.action: cApiPOST.setLike, cApiPOST.likeAction: cApiPOST.like, cApiPOST.postID: postID]
    }
    
    model.performVote(parametrs: parametrs!, like: like)
  }
  //-----------------------------------------------------------------------------------
}
