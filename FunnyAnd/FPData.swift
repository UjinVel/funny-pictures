/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Структуры данных постов
 */

import UIKit

//*** Виды категорий
enum EnRequest {
  case quotes
  case funnyPictures
  case memes
  case random
}

//*** Лайки
enum EnVote {
  case up
  case down
}

//*** Ответ на лайки
enum EnVoteSuccess {
  case up
  case down
  case alreadyVoted// уже голосовали
}

//*** Структура буфера данных
struct FPSCatData {
  var funny: [FPSPostData]  = []
  var memes: [FPSPostData]  = []
  var quotes: [FPSPostData] = []
  var random: [FPSPostData] = []
  
  mutating func removeAll() {
    funny.removeAll()
    memes.removeAll()
    quotes.removeAll()
    random.removeAll()
  }
  
  func isEmpty() -> Bool {
    return funny.isEmpty && memes.isEmpty && quotes.isEmpty && random.isEmpty
  }
}

//*** Данные изображения
struct FPSImageData {
  var aspectFitSize   = CGSize(width: 0, height: 0)// предварительный размер изображения после Aspect Fit
  var width: CGFloat  = 0// ширина картинки поста(приходит с сервера)
  var height: CGFloat = 0// высота картинки поста(приходит с сервера)
  var url             = URL(string: "")// адрес, по которому изображение храниться на сервере
  
  private var _url = ""// храниться предварительно обработаный адрес(строка)
  
  //*** При записи проеверяем не пустая ли строка и заменяем http на https
  var urlStr: String {
    set(http) {
      _url = httpsSet(url: http)// заменяем http на https
      
      guard let url_ = _url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
        assert(false, cErrorHandling.Messages.cantPercentEncoding)
      }
      
      guard let url__ = URL(string: url_) else {// проверка не пустой ли url
        assert(false, cErrorHandling.Messages.invalidImageURL)
      }
      
      url = url__
    }
    
    get {
      return _url
    }
    
  }
  //-----------------------------------------------------------------------------------
  
  //*** Заммена http на https
  private func httpsSet(url: String) -> String {
    do {
      let regex = try NSRegularExpression(pattern: cSpecialSymbols.http, options: .caseInsensitive)
      
      return regex.stringByReplacingMatches(in: url, options: .withTransparentBounds, range: NSMakeRange(0, url.characters.count), withTemplate: cSpecialSymbols.https)
    } catch {
        assert(false, cErrorHandling.Messages.NSRegularExpressionError)
      }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Проверка на валидность данных изображения
  func isImageDataOk() -> Bool {
    return !urlStr.isEmpty// && width > 0 && height > 0 - с сервера могут приходить нули
  }
  //-----------------------------------------------------------------------------------
  
  mutating func clear() {
    urlStr = ""
    width  = 0
    height = 0
  }
  //-----------------------------------------------------------------------------------
}

//*** Главная структура данных поста
struct FPSPostData: Equatable {
  var name                        = ""
  var link                        = ""// ссылка на пост, не на изображение
  var id: int_fast64_t            = 0// идентификатор поста
  var featuredMedia: int_fast64_t = 0// используеться для построения нового запроса и получения данных о картинке
  var likesCount: int_fast64_t    = 0// количество лайков поста
  var viewsCount: int_fast64_t    = 0// количество просмотров поста
  var category: EnRequest         = .funnyPictures// к какой категории принадлежит пост
  var Image                       = FPSImageData()// данные о картинке
  
  private var _tittle = ""
  
  //*** Описание поста. При записи замняем все спецсимволы
  var tittle: String {
    set(text) {
      _tittle = replaceSpecSymbols(text: text)
    }
    
    get {
      return _tittle
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Замена всех спецсимволов, если такие есть
  func replaceSpecSymbols(text: String) -> String {
    var res = text
    
    for (key, value) in cSpecialSymbols.symbols {
      res = res.replacingOccurrences(of: key, with: value)
    }
    
    return res
  }
  //-----------------------------------------------------------------------------------
  
  //*** Валидность данных постов
  func isPostDataOk() -> Bool {
    return !name.isEmpty && !tittle.isEmpty && !link.isEmpty// id и featuredMedia могут быть 0
  }
  //-----------------------------------------------------------------------------------
  
  //*** Equatable protocol
  public static func ==(lhs: FPSPostData, rhs: FPSPostData) -> Bool {
    if lhs.id == rhs.id {// сравниваем только id - шники
      return true
    }
    
    return false
  }
  //-----------------------------------------------------------------------------------
  
  mutating func clear() {
    name          = ""
    tittle        = ""
    link          = ""
    id            = 0
    featuredMedia = 0
    likesCount    = 0
    viewsCount    = 0
    Image         = FPSImageData()
  }
  //-----------------------------------------------------------------------------------
}
