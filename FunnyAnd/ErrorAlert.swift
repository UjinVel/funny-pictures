/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Вывод сообщения
 */

import UIKit

// DEBUG mode (установить флаг - "Swift Compiler - Custom Flags" => "Other Swift Flags" => -D DEBUG)

class ErrorAlert {
  
// MARK: - SHOW ALERT(if DEBUG mode only)
  
  static func getAlert(message: String) -> UIAlertController {
    let alertController = UIAlertController(title: cErrorHandling.Messages.alert, message: message, preferredStyle: .alert)
//    let defaultAction   = UIAlertAction(title: cErrorHandling.Messages.ok, style: .default) { action in
//      alertController.dismiss(animated: true, completion: nil)
//    }
//    
//    alertController.addAction(defaultAction)
    
    return alertController
  }
  //-----------------------------------------------------------------------------------
}

