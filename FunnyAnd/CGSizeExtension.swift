/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

extension CGSize {
  //*** Возвращает предварительный размер изображения после Aspect Fit, sourceSize - оригинальный размер изображения, self - target size
  func getImageProportionalSize(sourceSize: CGSize) -> CGSize {
    var targetHeight = height
    
    if targetHeight < sourceSize.height {
      targetHeight = sourceSize.height
    }
    
    var scaleFactor: CGFloat = 1
    var scaledWidth          = width
    var scaledHeight         = targetHeight
    
    if !__CGSizeEqualToSize(sourceSize, self) {
      let widthFactor  = width / sourceSize.width
      let heightFactor = targetHeight / sourceSize.height
      
      if widthFactor < heightFactor {
        scaleFactor = widthFactor
      } else {
          scaleFactor = heightFactor
        }
      
      scaledWidth  = sourceSize.width * scaleFactor
      scaledHeight = sourceSize.height * scaleFactor
    }
    
    return CGSize(width: scaledWidth, height: scaledHeight)
  }
  //-----------------------------------------------------------------------------------
}
