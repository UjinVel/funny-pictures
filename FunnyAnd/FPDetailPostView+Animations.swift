/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Анимации появления/исчезновения для детального отображения поста
 */

import UIKit

extension FPDetailPostView {

// MARK: FPDetailPostView show/hide
  
  //*** Анимация появления детального поста
  func performShowAnimation(indexPath: IndexPath, startFrame: CGRect, cell: FPBaseCell) {
    
// TODO: - убрать копипасту из show/hide!!!
    
    guard let parentViewCnt = parentViewController else {// получаем ссылку на родительский контроллер
      return
    }
    
    let minAlpha: CGFloat = 0
    let maxAlpha: CGFloat = 1
    let animationDuartion = cMainScreenPostsViewController.DetailPost.showHideAnimationDuration
    
    alpha    = minAlpha
    isHidden = false
    
    collectionViewDetailPost.performBatchUpdates( {
      // скролим к нужной ячейке, оборачиваем в performBatchUpdates() для отслеживания конца скрола(данные здесь уже есть)
      self.collectionViewDetailPost.scrollToItem(at: indexPath, at: .right, animated: false)
    }) { completition in
      self.setCurrCellData()// обновляем надписи
      
      // ячейка для детального отображения поста
      guard let detailPostCell = self.collectionViewDetailPost.cellForItem(at: indexPath) as? FPDetailPostCell else {
        assert(false, cErrorHandling.Messages.cantCastErr)
      }
      
      detailPostCell.layoutSubviews()// обновляем скролл внутри ячейки(нужно для установки корректного contentSize)
      
      let imageView         = UIImageView(image: detailPostCell.imageView.image)// создаем вью, которую будем анимировать
      imageView.contentMode = detailPostCell.imageView.contentMode
      imageView.frame       = startFrame// начальное положение - положение postCell.imageView в системе координат главной вьюхи FPMainScreenPostsViewController
      
      // получааем ссылку на базовый контроллер
      guard let baseController = self.getParentObject(FPBaseViewController.self) else {
        return
      }
      
      parentViewCnt.view.insertSubview(imageView, belowSubview: baseController.headerView!)// добавляем под хэдер, здесь он точно существует
      
      // скрываем изображения в обеих ячейках, после добавления вью для анимации
      cell.setImage(hiden: true)
      detailPostCell.imageView.isHidden = true
      
      // конечное положение вью такое же, как и detailPostCell.imageView, кроме высоты статус бара, т.к. добавили вью в parentViewCnt.view а не к self и смещение у скрола, если есть
      let x      = detailPostCell.imageView.frame.origin.x
      let y      = detailPostCell.imageView.frame.origin.y + UIApplication.shared.statusBarFrame.height - detailPostCell.scrollView.contentOffset.y
      let width  = detailPostCell.imageView.frame.width
      let height = detailPostCell.imageView.frame.height
      
      let endFrame = CGRect(x: x, y: y, width: width, height: height)
      
      UIImageView.animate(withDuration: animationDuartion, animations: {
        imageView.frame                  = endFrame
        self.alpha                       = maxAlpha
        baseController.headerView?.alpha = minAlpha// уменьшаем прозрачность хэдера отдельно, т.к. imageView находиться под ним
        
        self.layoutIfNeeded()
      }) { completition_ in
        imageView.removeFromSuperview()
        self.performShowHideFooterAndHeaderAnim()
        
        // делаем видимыми изображения в обеих ячейках
        detailPostCell.imageView.isHidden = false
        cell.setImage(hiden: true)
        
        parentViewCnt.setPan(enabled: false)// запрещаем пан для бокового меню
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Анимация исчезновения детального поста
  func performHideAnimation(indexPath: IndexPath, startFrame: CGRect, completitionBlock: (() -> Void)?) {
    let minAlpha: CGFloat            = 0
    let maxAlpha: CGFloat            = 1
    let scrollDuration: TimeInterval = 0// ставим 0 для анимации scrollToItem(выполняем в UIView.animate, для отслеживания конца выполнения scrollToItem)
    let animationDuartion            = cMainScreenPostsViewController.DetailPost.showHideAnimationDuration
    
    guard let parentViewCnt = parentViewController else {
      return
    }
    
    performHideFooterAndHeaderAnim(animated: false)
    
    // массив не будет пуст, если среди видимых ячеек постов находиться текущая
    let currCell = parentViewCnt.collectionViewPosts.visibleCells.filter{ parentViewCnt.collectionViewPosts.indexPath(for: $0) == indexPath }
    // выполняем scrollToItem в UIView.animate, чтобы отследить завершение скрола(не используем performBatchUpdates, т.к. вместе с layoutIfNeeded() в лог выводиться ошибка)
    UIView.animate(withDuration: scrollDuration, animations: {
      if currCell.isEmpty {// если проскролили ячейки детального поста, скролим туда же и ячейки collectionViewPosts
        parentViewCnt.collectionViewPosts.scrollToItem(at: indexPath, at: .bottom, animated: false)
        parentViewCnt.collectionViewPosts.layoutIfNeeded()
      }
    }) { completition in
      // ячейка поста
      guard let postCell = parentViewCnt.collectionViewPosts.cellForItem(at: indexPath) as? FPPostCell else {
        assert(false, cErrorHandling.Messages.cantCastErr)
      }
      // ячейка для детального отображения поста
      guard let detailPostCell = self.collectionViewDetailPost.cellForItem(at: indexPath) as? FPDetailPostCell else {
        assert(false, cErrorHandling.Messages.cantCastErr)
      }
      
      let endFrame          = postCell.imageView.getCoordinatesInView(parentViewCnt.view)// конечное положение - положение postCell.imageView в системе координат parentViewCnt.view
      let imageView         = UIImageView(image: postCell.imageView.image)// вьюха, которую будем анимировать
      imageView.contentMode = postCell.imageView.contentMode
      imageView.frame       = startFrame
      
      // получааем ссылку на базовый контроллер
      guard let baseController = self.getParentObject(FPBaseViewController.self) else {
        return
      }
      
      parentViewCnt.view.insertSubview(imageView, belowSubview: baseController.headerView!)// добавляем под хэдер, здесь он точно существует
      
      // скрываем изображения в обеих ячейках, после добавления вью для анимации
      postCell.imageView.isHidden       = true
      detailPostCell.imageView.isHidden = true
      
      UIImageView.animate(withDuration: animationDuartion, animations: {
        imageView.frame                  = endFrame!// endFrame здесь точно существует
        self.alpha                       = minAlpha
        baseController.headerView?.alpha = maxAlpha
        
        self.parentViewController?.view.layoutIfNeeded()
      }) { completition_ in
        imageView.removeFromSuperview()
        
        self.isHidden               = true// скрываем вьюху для детального отображения поста
        postCell.imageView.isHidden = false// делаем видимыми изображения в ячейке поста
        
        parentViewCnt.setPan(enabled: true)// разрешаем пан для бокового меню
        
        completitionBlock?()
      }
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: Header/footer show/hide
  
  //*** Анимация появления/исчезновения футера и хэдера в зависмости от текущего состояния
  func performShowHideFooterAndHeaderAnim() {
    var _footerBottom: CGFloat = 0
    var _headerTop: CGFloat    = 0
    let _constants             = cMainScreenPostsViewController.DetailPost.self
    
    if footerBottom.constant == 0 {
      _footerBottom = footerView.frame.height
    }
    
    if headerTop.constant == 0 {
      _headerTop = -headerView.frame.height
    }
    
    UIView.animate(withDuration: _constants.showHideFooterHeaderDuration) {
      self.headerTop.constant    = _headerTop
      self.footerBottom.constant = _footerBottom
      
      self.layoutIfNeeded()
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Анимация исчезновения футера и хэдера
  func performHideFooterAndHeaderAnim(animated: Bool) {
    var duration: TimeInterval = 0
    let _constants             = cMainScreenPostsViewController.DetailPost.self
    
    if animated {
      duration = _constants.showHideFooterHeaderDuration
    }
    
    DispatchQueue.main.async {
      UIView.animate(withDuration: duration) {
        self.headerTop.constant    = -self.headerView.frame.height
        self.footerBottom.constant = self.footerView.frame.height
        
        self.layoutIfNeeded()
      }
    }
  }
  //-----------------------------------------------------------------------------------
}
