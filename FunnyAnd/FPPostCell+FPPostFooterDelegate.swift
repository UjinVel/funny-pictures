/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Обработка событий нажатий кнопок на футере ячейки
 */

import UIKit

extension FPPostCell: FPPostFooterDelegate {
  //*** Лайкнули
  func postFooterVoteUp() {
    guard let parentController = getParentObject(FPMainScreenPostsViewController.self) else {
      assert(false, cErrorHandling.Messages.noParentVC)
    }
    
    if tag > parentController.postData.count {
      assert(false, cErrorHandling.Messages.outOfRange)
    }
    
    if !parentController.presenter.isDataDownloadedAtThisMoment {
      // запрос на лайк
      parentController.presenter?.vote(like: .up, postID: parentController.postData[tag].id, postIndex: tag)
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Дислайкнули
  func postFooterVoteDown() {
    guard let parentController = getParentObject(FPMainScreenPostsViewController.self) else {
      assert(false, cErrorHandling.Messages.noParentVC)
    }
    
    if tag > parentController.postData.count {
      assert(false, cErrorHandling.Messages.outOfRange)
    }
    
    if !parentController.presenter.isDataDownloadedAtThisMoment {
      // запрос на дислайк
      parentController.presenter?.vote(like: .down, postID: parentController.postData[tag].id, postIndex: tag)
    }
  }
  //-----------------------------------------------------------------------------------
  
  //*** Загружаем картинку поста
  func postFooterDownloadImage() -> UIImage {
    return imageView.image!// если нет изображения, сохраним плейсхолдер
  }
  //-----------------------------------------------------------------------------------
  
  //*** Шара
  func postFooterShare() -> (tittle: String, link: String, controller: UIViewController) {
    guard let parentController = getParentObject(FPMainScreenPostsViewController.self) else {
      assert(false, cErrorHandling.Messages.noParentVC)
    }
    
    if tag > parentController.postData.count {
      assert(false, cErrorHandling.Messages.outOfRange)
    }
    
    let link = parentController.postData[tag].link
    let text = parentController.postData[tag].tittle
    
    return (text, link, parentController)
  }
  //-----------------------------------------------------------------------------------
}
