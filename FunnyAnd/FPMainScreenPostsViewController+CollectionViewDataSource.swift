/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * UICollectionViewDataSource для главного экрана и детального поста
 */

import UIKit

extension FPMainScreenPostsViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let defCellsCount = 3// возвращаем несколько ячеек, чтобы работал скролл
    
    if !postData.isEmpty {
      collectionView.backgroundView?.isHidden = true
      
      return postData.count
    } else if !presenter.isDataDownloadedAtThisMoment {// Отображаем сообщение, если таблица пуста и данные не загружаються в данный момент
        let messageLabel = UILabel(frame: CGRect(x: cMainScreenPostsViewController.NoDataLabel.xPos,
                                                 y: cMainScreenPostsViewController.NoDataLabel.yPos,
                                                 width: view.frame.size.width,
                                                 height: view.frame.size.height))
      
        messageLabel.text          = cMainScreenPostsViewController.NoDataLabel.text
        messageLabel.textColor     = .black
        messageLabel.numberOfLines = cMainScreenPostsViewController.NoDataLabel.nuberOfLines
        messageLabel.textAlignment = .center
        messageLabel.font          = UIFont(name: cMainScreenPostsViewController.NoDataLabel.fontName, size: cMainScreenPostsViewController.NoDataLabel.fontSize)
      
        messageLabel.sizeToFit()
      
        collectionView.backgroundView = messageLabel
      } else {// отображаем индикатор загрузки
          let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
      
          activityIndicator.frame = CGRect(x: view.frame.width / 2, y: view.frame.height / 2, width: 0, height: 0)
      
          activityIndicator.startAnimating()
      
          collectionView.backgroundView = activityIndicator
        }
    
    
    collectionView.backgroundView?.isHidden = false
    
    if collectionView == collectionViewPosts {
      return defCellsCount
    } else {
        return 0
      }
  }
  //-----------------------------------------------------------------------------------
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView == collectionViewPosts {
      guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cMainScreenPostsViewController.Reuseidentifiers.postsCell, for: indexPath) as? FPPostCell else {
        assert(false, cErrorHandling.Messages.cantCastErr)
      }
    
      if !postData.isEmpty {
        cell.isHidden = false
      
        cell.setup(data: postData[indexPath.row], index: indexPath.row)
      } else {
          cell.isHidden = true
        }
    
      return cell
    } else {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cMainScreenPostsViewController.Reuseidentifiers.detailPostCell, for: indexPath) as? FPDetailPostCell else {
          assert(false, cErrorHandling.Messages.cantCastErr)
        }
      
        cell.setup(data: postData[indexPath.row], indexPath: indexPath)
      
        return cell
      }
  }
  //-----------------------------------------------------------------------------------
  
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    switch kind {
      case UICollectionElementKindSectionHeader:// хэдер
        guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: cMainScreenPostsViewController.Reuseidentifiers.headerView, for: indexPath) as? FPCollectionPostsReusableHeaderView else {
          assert(false, cErrorHandling.Messages.cantCastErr)
        }
        
        headerView.initialSetup(data: categoriesData)
        
        return headerView
      case UICollectionElementKindSectionFooter:// футер
        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: cMainScreenPostsViewController.Reuseidentifiers.footerView, for: indexPath)
      
        // отключаем анимацию индикатора загрузки, если нет данных
        for indicator in footerView.subviews {
          if let indicatorActivity = indicator as? UIActivityIndicatorView {
            if postData.isEmpty {
              indicatorActivity.stopAnimating()// в сториборде стоит hide when stopping
            } else {
                indicatorActivity.startAnimating()
              }
          
            break
          }
        }
      
        return footerView
      default:
        assert(false, cErrorHandling.Messages.viewForSupplementaryElementOfKindErr)
    }
  }
  //-----------------------------------------------------------------------------------
}
