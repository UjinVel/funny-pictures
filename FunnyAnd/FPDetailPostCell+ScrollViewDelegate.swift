/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * UIScrollViewDelegate для скрола ячейки FPDetailPostCell
 */

import UIKit

extension FPDetailPostCell: UIScrollViewDelegate {
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return scrollContentView
  }
  //-----------------------------------------------------------------------------------
  
  func scrollViewDidZoom(_ scrollView: UIScrollView) {
    if scrollView.zoomScale != _constants.zoomScale {// если увеличили изображение пан не активен
      setPan(enabled: false)
    } else if scrollView.contentOffset.y > frame.height {
        setPan(enabled: true)
      }
    
    if scrollView.zoomScale < _constants.zoomScale {
      scrollView.zoomScale = _constants.zoomScale
    }
  }
  //-----------------------------------------------------------------------------------
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    guard scrollView.zoomScale == _constants.zoomScale else {
      return
    }
    
    let boundY = lroundf(Float(scrollView.contentSize.height - frame.height))
    if scrollView.contentOffset.y >= CGFloat(boundY) || scrollView.contentOffset.y <= 0 {
      // пан активен если доскролили до верхнего или нижнего края изображения
      setPan(enabled: true)
      
      imageViewYPos.constant = _imageViewYPos
    } else {
        setPan(enabled: false)
      }
  }
  //-----------------------------------------------------------------------------------
}
