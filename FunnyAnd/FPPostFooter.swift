/*
 * Copyright (c) Ujin Velichko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Вьюха - футер поста(кнопки поделиться, лайк, дизлайк, сохранить, картинку)
 */

import UIKit

//*** События нажатия кнопок
protocol FPPostFooterDelegate: class {
  func postFooterVoteUp()// лайкнули
  func postFooterVoteDown()// дислайкнули
  func postFooterDownloadImage() -> UIImage// зангрузка картинки
  func postFooterShare() -> (tittle: String, link: String, controller: UIViewController)// шаринг
}

class FPPostFooter: UIView {
  @IBOutlet weak var downloadButton: UIButton!
  @IBOutlet weak var downloadActivityIndicator: UIActivityIndicatorView!
  
  weak var delegate: FPPostFooterDelegate?
  
// MARK: - Initial setup
  
  //*** Позицианируем вьюху с auto layout
  func setupPosition(superView: UIView) {
    if !superView.subviews.contains(self) {
      superView.addSubview(self)
    }
    
    let constants = cMainScreenPostsViewController.PostFooter.self
    
    translatesAutoresizingMaskIntoConstraints = false
    
    // право
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .trailing,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .trailing,
                                               multiplier: constants.trailingMultiplier,
                                               constant: constants.trailingConstant))
    // лево
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .leading,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .leading,
                                               multiplier: constants.leadingMultiplier,
                                               constant: constants.leadingConstant))
    // вверх
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .top,
                                               multiplier: constants.topMultiplier,
                                               constant: constants.topConstant))
    // низ
    superView.addConstraint(NSLayoutConstraint(item: self,
                                               attribute: .bottom,
                                               relatedBy: .equal,
                                               toItem: superView,
                                               attribute: .bottom,
                                               multiplier: constants.bottomMultiplier,
                                               constant: constants.bottomConstant))
  }
  //-----------------------------------------------------------------------------------
  
  //*** Обработка события конца загрузки картинки
  func imageSavedToPhotosAlbum(image: UIImage, didFinishSavingWithError: NSError?, contextInfo: UnsafeRawPointer) {
    // увеличиваем время анимации загрузки картинки на cMainScreenPostsViewController.PostFooter.downloadAnimDuration
    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(cMainScreenPostsViewController.PostFooter.downloadAnimDuration)) {
      self.downloadButton.isHidden = false
      self.downloadActivityIndicator.stopAnimating()
    }
    
    if didFinishSavingWithError != nil {
      print(didFinishSavingWithError!.code)
    }
  }
  //-----------------------------------------------------------------------------------
  
// MARK: - button actions
  
  //*** Начинаем загрузку картинки
  @IBAction func downloadImage(_ sender: UIButton) {
    guard let savedImage = delegate?.postFooterDownloadImage() else {
      return
    }
    
    downloadButton.isHidden = true
    downloadActivityIndicator.startAnimating()
    
    UIImageWriteToSavedPhotosAlbum(savedImage, self,  #selector(imageSavedToPhotosAlbum(image: didFinishSavingWithError: contextInfo:)), nil)
//    чтобы работала загрузка, нужно добавить в info.plist
//    <key>NSPhotoLibraryUsageDescription</key>
//    <string>$(PRODUCT_NAME) uses photos</string>
  }
  //-----------------------------------------------------------------------------------

  //*** Дислайкнули
  @IBAction func voteDown(_ sender: UIButton) {
    delegate?.postFooterVoteDown()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Лайкнули
  @IBAction func voteUp(_ sender: UIButton) {
    delegate?.postFooterVoteUp()
  }
  //-----------------------------------------------------------------------------------
  
  //*** Шара
  @IBAction func share(_ sender: UIButton) {
    guard let data = delegate?.postFooterShare() else {
      return
    }
    
    let sourceRectY: CGFloat = -10// слегка поднимаем activityVC по оси Y, чтобы не наезжать на кнопку
    let activityVC           = UIActivityViewController(activityItems: [data.link, data.tittle], applicationActivities: nil)
    
    if activityVC.popoverPresentationController != nil {
      activityVC.popoverPresentationController?.sourceView               = sender
      activityVC.popoverPresentationController?.permittedArrowDirections = .down
      activityVC.popoverPresentationController?.sourceRect               = CGRect(x: sender.frame.width / 2, y: sourceRectY, width: 0, height: 0)
    }
    
    data.controller.present(activityVC, animated: true, completion: nil)
  }
  //-----------------------------------------------------------------------------------
}
